jQuery(document).ready(function($) {
//ロールオーバー
	var postfix = '_on';
	$('.over').not('[src*="'+ postfix +'."]').each(function() {

		var img = $(this);
		var src = img.attr('src');
		var src_on = src.substr(0, src.lastIndexOf('.'))
		           + postfix
		           + src.substring(src.lastIndexOf('.'));
		$('<img>').attr('src', src_on);
		img.hover(
			function() {
				img.attr('src', src_on);
			},
			function() {
				img.attr('src', src);
			}
		);
	});

	var postfix = '_c';
	$('.primary_current').not('[src*="'+ postfix +'."]').each(function() {

		var img = $(this);
		var src = img.attr('src');
		var src_c = src.substr(0, src.lastIndexOf('.'))
		           + postfix
		           + src.substring(src.lastIndexOf('.'));
		$('<img>').attr('src', src_c);
		img.attr('src', src_c);
	});

//スムーズスクロール
	jQuery.easing.quart = function (x, t, b, c, d) {
		return -c * ((t=t/d-1)*t*t*t - 1) + b;
	};  
	$('.pageTop a').click(function () {
		$('html,body').animate({ scrollTop: 0 }, 1000, 'quart');
		return false;
	});

//ファンクションの起動
	js_window_open();
	js_lNavi_visual();

});

/*	//	PAGE PRINT
-----------------------------------------------------------------------------------------------------------------*/
function js_page_print(){
	window.print();
	return false
}
/*	//	ウィンドウを閉じる - a要素にclass="js_window_close"でウィンドウを閉じる
-----------------------------------------------------------------------------------------------------------------*/
function js_window_close(){
	window.close();
	return false;
}
/*	//	ポップアップ - a要素にclass="js_window_open-width-height"でポップアップ
-----------------------------------------------------------------------------------------------------------------*/
function js_window_open(){
	var js_para = null;
	// js_para[0] = width
	// js_para[1] = height
	// js_para[2] = window.name
	$('a[class^="js_window_open"], area[class^="js_window_open"]').each(function(index){
		$(this).click(function(){
			var wo = null;
			// get window width & height
			js_para = $(this).attr('class').match(/[0-9]+/g);
			// get window.name
			window.name ? js_para[2] = window.name+'_' : js_para[2] = ('');
			wo = window.open(this.href, js_para[2]+'popup'+index,'width='+js_para[0]+',height='+js_para[1]+',scrollbars=yes');
			wo.focus();
			return false;
		});
	});
}
/*	//	js_Navi_setup
-----------------------------------------------------------------------------------------------------------------*/
function js_lNavi_visual(){
	var _root_elm = $('body').attr('class');
	$('#lNavi ul ul').hide();

	if(_root_elm.indexOf("lDef") < 0){
		$('#lNavi').each(function(){

			_routing = new Array;
			_routing[0] = _root_elm.match(/l[\d]+_[\d]+_[\d]+/);
			_routing[1] = _root_elm.match(/l[\d]+_[\d]+/);
			_routing[2] = _root_elm.match(/l[\d]+/);
			if(_routing[0]){ _routing[0].push(_routing[0][0].match(/[\d]+/g))};
			if(_routing[1]){ _routing[1].push(_routing[1][0].match(/[\d]+/g))};
			if(_routing[2]){ _routing[2].push(_routing[2][0].match(/[\d]+/g))};
//console.log(_routing[0]);
//console.log(_routing[1]);
//console.log(_routing[2][1]);

			if(_routing[0] != null){
				$('a.lNav'+_routing[0][1][0]+'_'+_routing[0][1][1]+'_'+_routing[0][1][2])
				.addClass('current')
				.parent().parent().show()
				.parent().parent().show();
	
			}else if(_routing[1] != null){
				$('a.lNav'+_routing[1][1][0]+'_'+_routing[1][1][1])
				.addClass('current')
				.next().show()
				.parent().parent().show();
		
			}else if(_routing[2] != null){
				$('a.lNav'+_routing[2][1])
				.addClass('current')
				.next().show();
			}else{
			}
		
		});
	}else{
	}
}

$(function(){
		$(".opacityOver").fadeTo(1,1);
		$(".opacityOver").hover(
		  function () {
			$(this).stop().fadeTo(200, 0.7);
		  },
		  function () {
			$(this).stop().fadeTo(400, 1);
		  }
		);
});

/*	//	Firefox用CSSを呼び出し（ルート相対パスなのでサーバー上でのみ）
-----------------------------------------------------------------------------------------------------------------*/
var firefox = (navigator.userAgent.indexOf("Firefox") != -1)? true : false; 
if(firefox) document.write('<link rel="stylesheet" type="text/css" media="print" href="/common/css/fx_print.css" />');




/* window open */
function openWin(url,title,wdh,hgt,opt) {
	var win;
	if (url) {
		if (!title) title = "_blank";
		if (!opt) {
			var opti = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,favorites=yes";
		} else if (opt=="1") {
			var opti = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,resizable=yes,favorites=no";
		} else if (opt=="2") {
			var opti = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,favorites=no";
		} else if (opt=="3") {
			var opti = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no,favorites=no";
		} else if (opt=="4") {
			<!-- MMTV -->
			var opti = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,favorites=no";
			wdh = "950";
			hgt = "635";
		} else if (opt=="5") {
			<!-- 外部サイトリンク -->
			var opti = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,resizable=yes,favorites=no";
			wdh = "595";
			hgt = "476";
		} else if (opt=="6") {
			<!-- oneline Simu -->
			var opti = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,favorites=no,width=800,height=620,top=30,left=10";
			//wdh = "595";
			//hgt = "476";
		} 
		
		if(!!wdh&&!!hgt) opti+=",width="+wdh+",height="+hgt;
		win = window.open(url,title,opti);
		win.focus();
	}
}


/*jQuery(function(){
    $("#main_headmenu").load("/component/common/main_header.html");
	$("#sidemenu_lineup").load("/component/common/sidemenu_lineup.html");
	$("#sidemenu_purchase").load("/component/common/sidemenu_purchase.html");
	$("#sidemenu_lineup").load("/component/common/sidemenu_lineup.html");
	$("#sidemenu_maintenance").load("/component/common/sidemenu_maintenance.html");
	$("#sidemenu_accessory").load("/component/common/sidemenu_accessory.html");
	$("#sidemenu_special").load("/component/common/sidemenu_special.html");
	$("#sidemenu_navi").load("/component/common/sidemenu_navi.html");
	$("#sidemenu_reference").load("/component/common/sidemenu_reference.html");
	$("#footer_main").load("/component/common/footer_main.html", callback_release_function);
	$("#footer_main_top").load("/component/common/footer_main_top.html", callback_release_function);
	$(".news_purchase").load("/component/documents/news_purchase.html");
	/*$(".news_releases").load("/component/documents/news_releases.html");
	$(".news_message").load("/component/documents/news_message.html");
	
	function callback_release_function() {
    $(".news_releases").load("/component/documents/news_releases.html");
	$(".news_message").load("/component/documents/news_message.html");
	}

});
*/
