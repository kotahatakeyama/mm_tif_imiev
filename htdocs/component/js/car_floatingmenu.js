
/* ---------------------------------------------
		Setting 
--------------------------------------------- */

var car_floatingmenu_able = true;

if(window.navigator.appVersion.toLowerCase().indexOf('msie 6') > 0) car_floatingmenu_able = false;

function iosVersion() {
    var v, versions;
    if ( /iP(hone|od|ad)/.test( navigator.platform ) ) {
        // supports iOS 2.0 and later: &lt;http://bit.ly/TJjs1V&gt;
        v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
        versions = [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
        return versions[ 0 ];
    }
    return versions;
}
if( typeof iosVersion() !== "undefined" && iosVersion() < 5) car_floatingmenu_able = false;


/* CSS File load */

function ftm_prntCSS() {
	var d = document;
	var link = d.createElement('link');
		link.href = '/component/css/car_floatingmenu.css';
		link.rel = 'stylesheet';
		link.type = 'text/css';
	var h = d.getElementsByTagName('head')[0];
		h.appendChild(link);
}

if(car_floatingmenu_able) ftm_prntCSS();

/* ---------------------------------------------
		Floating Menu 
--------------------------------------------- */

var car_floatMenu = {
	
	$ftNavi : '',
	
	$ftmBtm : '',
	$ftmPtb : '',
	
	ftm_bottom: false,
	ftMenu : [],
	
	mm_flg : true,
	mm_url : 'http://www.mitsubishi-motors.co.jp/special/entertainment/mailmagazine/mmmm/index.html',
	fb_flg : false,
	fb_url : '#',
	
	banner : { name: '', image: '', url : '', target : ''},
	
	prImg_ary : [
		'/component/images/floatingmenu/ftm_pagetop_bg.png'
	 ],
	
	init : function(){
		
		if(typeof(floatingMenu_item.menu) != 'undefined') {
			for(var i=0; i<floatingMenu_item.menu.length; i++) {
				if(floatingMenu_item.menu[i].enable) this.ftMenu.push(floatingMenu_item.menu[i]);
			}
		}
		
		for(var i=0; i<this.ftMenu.length; i++) {
			if(this.ftMenu[i].image != '') this.prImg_ary.push(this.ftMenu[i].image);
		}
		
		var prImg= new Array();
		for (i=0; i<this.prImg_ary.length; i++) {
			prImg[i] = new Image();
			prImg[i].src = this.prImg_ary[i];
		}		
		
		if(typeof(floatingMenu_item.mailmagazine) != 'undefined') {
			this.mm_flg = floatingMenu_item.mailmagazine;
		}
		
		if(typeof(floatingMenu_item.facebook) != 'undefined') {
			this.fb_flg = floatingMenu_item.facebook;
		}
		
		if(typeof(floatingMenu_item.banner) != 'undefined') {
			if(typeof(floatingMenu_item.banner.name) != 'undefined') this.banner.name = floatingMenu_item.banner.name;
			if(typeof(floatingMenu_item.banner.image) != 'undefined') this.banner.image = floatingMenu_item.banner.image;
			if(typeof(floatingMenu_item.banner.url) != 'undefined') this.banner.url = floatingMenu_item.banner.url;
			if(typeof(floatingMenu_item.banner.target) != 'undefined') this.banner.target = (floatingMenu_item.banner.target == '_blank')? '_blank' : '_self';
		}
		
		var key = 0;
		var intvl = setInterval(function(){
			//console.log('key = '+$('#carlocal_menu_sub').length)
			if($('#ftNavi').length > 0) key = 1;
			if(key == 1) {
				clearInterval(intvl);
				car_floatMenu.$ftNavi = $('#ftNavi');
				car_floatMenu.mkMenu();
			}
		}, 100);
		
	},
	
	mkMenu : function(){
		
		/*  Floating Menu Bottom  */
		
		$('body').append('<div id="ftm-bottom"><div class="ftm-wrapper"><div class="ftm-inner"><div class="ftm-bg"></div><div class="ftm-blk"><ul></ul></div></div></div></div>');
		this.$ftmBtm = $('#ftm-bottom');
		
		for (var i=0; i<this.ftMenu.length; i++) {
			var name = (typeof(this.ftMenu[i].name) != 'undefined' )? this.ftMenu[i].name : '';
			var image = (typeof(this.ftMenu[i].image) != 'undefined' )? this.ftMenu[i].image : '';
			var lnk = (typeof(this.ftMenu[i].url) != 'undefined' )? this.ftMenu[i].url : '';
			var tgt = (typeof(this.ftMenu[i].target) != 'undefined' )? this.ftMenu[i].target : '';
				tgt = (tgt == '_blank')? '_blank': '_self';
			
			var trficCode = '';
			if(typeof(this.ftMenu[i].log_code) != 'undefined' ) {
				if(this.ftMenu[i].log_code[1] == '' || log_code.length == 1) {
					if(this.ftMenu[i].log_code[0] != '') trficCode = 'onclick="adtrafficTrack._trackFlashRequest(\''+this.ftMenu[i].log_code[0]+'\');"';
				} else if(this.ftMenu[i].log_code.length == 2) {
					if(this.ftMenu[i].log_code[0] != '') trficCode = 'onclick="adtrafficTrack._trackFlashRequest(\''+this.ftMenu[i].log_code[0]+'\', \''+this.ftMenu[i].log_code[1]+'\');"';
				}
			}
			//console.log(trficCode);

			var code = '<li id="ftm-b-btn'+(i+1)+'" class="list-ftm-m1"><a href="'+lnk+'" '+trficCode+' target="'+tgt+'" style="background-image:url('+image+');">'+name+'</a></li>';
			car_floatMenu.$ftmBtm.find('.ftm-blk ul').append(code);
		}
			
		
		if(this.banner.image != '') {
			var code = '<li id="ftm_btn_bn"><a href="'+this.banner.url+'" target="'+this.banner.target+'"><img src="'+this.banner.image+'" alt="'+this.banner.name+'" width="139" height="38" /></a></li>';
			this.$ftmBtm.find('.ftm-blk ul').append(code);
		}
		if(this.mm_flg) this.$ftmBtm.find('.ftm-blk ul').append('<li id="ftm_btn_mm" class="list-ftm-m2"><a href="'+this.mm_url+'" target="_blank">メールマガジン</a></li>');
		if(this.fb_flg) this.$ftmBtm.find('.ftm-blk ul').append('<li id="ftm_btn_fb" class="list-ftm-m2"><a href="'+this.fb_url+'" target="_blank">公式Facebook</a></li>');
		
		this.$ftmBtm.css({'display': 'none'});
		
		
		/*  Floating Menu PageTop  */
		$('body').append('<div id="ftm-pagetop-btn" class="pgTop"><a href="#">ページトップへ</a></div>');
		
		this.$ftmPtb = $('#ftm-pagetop-btn');		
		this.$ftmPtb.find('a').click(function(){
			$('body,html').animate({'scrollTop':0}, 600, 'swing');
			return false;
		});
		this.$ftmPtb = $('#ftm-pagetop-btn');
		
		this.setEvnt();
	},
	
	setMenu : function(){
		
		var winH = $(window).height();
		var winW = $(window).width();
		var scrlTop = $(window).scrollTop();
		var scrlLft = $(window).scrollLeft();
		
		//var hdMenuTop = car_floatMenu.$hdNavi.offset().top + car_floatMenu.$hdNavi.height();
		var ftMenuTop = car_floatMenu.$ftNavi.offset().top;
		//var top_din = scrlTop - hdMenuTop;
		var btm_din = ftMenuTop - (scrlTop+winH) ;

		if(btm_din > 54) {
			if( !car_floatMenu.ftm_bottom ) {
				//console.log('BOTTOM-OPEN');
				car_floatMenu.ftm_bottom = true;
				car_floatMenu.$ftmBtm.css({'display':'block'});
				car_floatMenu.$ftmBtm.find('.ftm-inner').stop();
				car_floatMenu.$ftmBtm.find('.ftm-inner').animate({'top':  0}, 300);
				
				car_floatMenu.$ftmPtb.stop();
				car_floatMenu.$ftmPtb.fadeIn(300).animate({'bottom': 66}, 300);
			}
		} else {
			if( car_floatMenu.ftm_bottom ) {
				//console.log('BOTTOM-CLOSE');
				car_floatMenu.ftm_bottom = false;
				car_floatMenu.$ftmBtm.find('.ftm-inner').stop();
				car_floatMenu.$ftmBtm.find('.ftm-inner').animate({'top': 54}, 300, function(){
					car_floatMenu.$ftmBtm.css({'display':'none'});
				});
				car_floatMenu.$ftmPtb.stop();
				car_floatMenu.$ftmPtb.animate({'bottom': 12}, 300);
			}
		}

		if(winW < 990) {
			car_floatMenu.$ftmBtm.find('.ftm-blk').css({'margin-left': -495-scrlLft});
		} else {
			car_floatMenu.$ftmBtm.find('.ftm-blk').css({'margin-left': -495});
		}
		//console.log('btm_din = '+btm_din +' /top_din = '+top_din + ' / scrlLft = '+scrlLft);
	},
	
	setEvnt : function() {
		setTimeout(function(){
			car_floatMenu.setMenu();
		}, 150)

		$(window).bind('scroll resize', function(){
			car_floatMenu.setMenu();
		});
	}
};

/* ---------------------------------------------
		Document Ready 
--------------------------------------------- */

$(function(){
	
	if( typeof(floatingMenu_item) == "undefined") {
		floatingMenu_item = {
			facebook : true,
			mailmagazine : true,
			banner : {
				image  : '',
				url    : '',
				target : ''
			}
		}
	}
	if(car_floatingmenu_able) car_floatMenu.init();
});
