// JavaScript Document

jQuery(function(){
    $("#main_headmenu").load("/component/common/main_header.html");
	$("#sidemenu_lineup").load("/component/common/sidemenu_lineup.html");
	$("#sidemenu_purchase").load("/component/common/sidemenu_purchase.html");
	$("#sidemenu_maintenance").load("/component/common/sidemenu_maintenance.html");
	$("#sidemenu_accessory").load("/component/common/sidemenu_accessory.html");
	$("#sidemenu_special").load("/component/common/sidemenu_special.html");
	$("#sidemenu_navi").load("/component/common/sidemenu_navi.html");
	$("#sidemenu_none").load("/component/common/sidemenu_none.html");
	
	$(".news_purchase").load("/component/documents/news_purchase.html");
	$(".news_mmtv").load("/component/documents/news_mmtv.html");
	$(".news_lineup").load("/component/documents/news_lineup.html");
	$(".news_maintenance").load("/component/documents/news_maintenance.html");
	$(".news_accessory").load("/component/documents/news_accessory.html");
	$(".news_navi").load("/component/documents/news_navi.html");
	$(".news_reference").load("/component/documents/news_reference.html");
	$(".news_special").load("/component/documents/news_special.html");
	$(".news_recall").load("/component/documents/news_recall.html");
	$(".news_heartyrun").load("/component/documents/news_heartyrun.html");
	
	$("#footer_main").load("/component/common/footer_main.html", callback_release_function);
	$("#footer_main_top").load("/component/common/footer_main_top.html", callback_release_function);
	
	function callback_release_function() {
    $(".news_releases").load("/component/documents/news_releases.html");
	$(".news_message").load("/component/documents/news_message.html");
	}
	
});


jQuery(function() {
	$(".accordionBox").hide();
	$('.accordion01_head').click(function(){
    $(this).next().slideToggle('fast');
	$('p', this).toggleClass("accordion_downs");
	})
	$('.accordion01_head').hover(function () {
  	$('p', this).css({ backgroundColor:"#ccc"});
		}, function () {
  		var cssObj = {
    	backgroundColor: "#FFF"
  	}
  $('p', this).css(cssObj);
});
});
                                      
 