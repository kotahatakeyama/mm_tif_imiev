var topic_cnt=0;
var topic_htmltag="";
var message_htmltag="";
var release_htmltag="";
var func_rm=0;

$(function(){
	$.ajax({
		url:'/component/parts/top_topic_data.xml',
		async: true,
		cache: false,
		type:'get',
		dataType:'xml',
		success:into_top_topic
	});

	$.ajax({
		url:'/component/parts/message_data.xml',
		async: true,
		cache: false,
		type:'get',
		dataType:'xml',
		success:into_message
	});

	$.ajax({
		url:'/component/parts/release_data.xml',
		async: true,
		cache: false,
		type:'get',
		dataType:'xml',
		success:into_release
	});
});

function into_top_topic(xml,status){
	if(status!='success')return;
	$(xml).find('item').each(top_topic_xml);
}

function into_message(xml,status){
	if(status!='success')return;
	$(xml).find('item').each(message_xml);
	$('#message_info').html(message_htmltag);
scroll_func();
}

function into_release(xml,status){
	if(status!='success')return;
	$(xml).find('item').each(release_xml);
	$('#release_info').html(release_htmltag);
scroll_func();
}

function top_topic_xml() {
topic_cnt=topic_cnt+1;
	var $url = $(this).find('url').text();
	var $target = $(this).find('target').text();
	var $image = $(this).find('image').text();
	var $title = $(this).find('title').text();
	var $sentence = $(this).find('sentence').text();

	if($target == "_blank"){
		topic_htmltag += '<li><a href="' + $url + '" target="_blank"><img src="/component/images/topics/' + $image + '" alt="トピックス" class="imgBorder" /><strong>' + $title + '</strong><br />' + $sentence + '</a></li>';
	} else {
		topic_htmltag += '<li><a href="' + $url + '"><img src="/component/images/topics/' + $image + '" alt="トピックス" class="imgBorder" /><strong>' + $title + '</strong><br />' + $sentence + '</a></li>';
	}

	switch (topic_cnt){
		case 6:
			$('#topic_img1').html(topic_htmltag);
			topic_htmltag="";
			break;
		case 12:
			$('#topic_img2').html(topic_htmltag);
			topic_htmltag="";
			break;
		case 18:
			$('#topic_img3').html(topic_htmltag);
			topic_htmltag="";
			break;
		case 24:
			$('#topic_img4').html(topic_htmltag);
			topic_htmltag="";
			break;
	}
}


function message_xml() {
	var $day = $(this).find('day').text();
	var $title = $(this).find('title').text();
	var $url = $(this).find('url').text();
	var $daytitle = $(this).find('daytitle').text();
	var $target = $(this).find('target').text();
	var $newmark = $(this).find('newmark').text();

	if($newmark == "NEW") {
		message_htmltag += '<li class="new">';
	} else {
		message_htmltag += '<li>';
	}

	if($url == "no_URL") {
		message_htmltag += '<table style="border-style:none;margin-top:0;"><tr><td valign="top" style="width:9.5em;border-style:none;padding:0;">';
		if($daytitle == "NO"){
			message_htmltag += $day + '<\/td><td style="border-style:none;padding:0;">';
		}else{
			message_htmltag += $daytitle + '<\/td><td style="border-style:none;padding:0;">';
		}
	} else {
		message_htmltag +='<span>'
		if($daytitle == "NO"){
			message_htmltag += $day + '<\/span>';
		}else{
			message_htmltag += $daytitle + '<\/span>';
		}
		if($target == "_blank"){
			message_htmltag += '<a href="'+$url+'" target="_blank">'
		}else{
			message_htmltag += '<a href="'+$url+'">'
		}
	}

	if($url == "no_URL") {
		message_htmltag += $title + '<\/td><\/tr><\/table><\/li>\n'
	} else {
		message_htmltag += $title + '<\/a><\/li>\n'
	}
}

function release_xml() {
	var $day = $(this).find('day').text();
	var $title = $(this).find('title').text();
	var $url = $(this).find('url').text();
	var $newmark = $(this).find('newmark').text();

	if($newmark == "NEW") {
		release_htmltag += '<li class="new"><span>' + $day + '<\/span><a href="'+$url+'" target="_blank">'+ $title + '<\/a><\/li>';
	} else {
		release_htmltag += '<li><span>' + $day + '<\/span><a href="'+$url+'" target="_blank">'+ $title + '<\/a><\/li>';
	}
}

function scroll_func() {
func_rm+=1;
	if(func_rm==2){

		$('#now_loading').css('display', 'none');
		$('#footer_container').fadeIn(500);
		scroll_add();
	}
}

function scroll_add()
	{
	$('.top_frames').jScrollPane({
		verticalDragMinHeight:15
	});
}