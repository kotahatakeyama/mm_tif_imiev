
/* carlineup search v0.95*/
(function(){
var comp = new Array();

var popuptxt = new Array();
var one = true;
var focusflag = false;
var focusclick = 0;
var ret;
var befheight;

ukk = {};

ukk.$ = function(IDS){
	if(ukk.stringChk(IDS)) return document.getElementById(IDS);
	else return null;
}
ukk.stringChk = function(object){
	if(typeof object == 'string' || object instanceof String) return object;
	return null;
}

ukk.getElementsByTagNameArray = function(tagname,element){
	if(typeof element == 'undefined') var doc = document;
	else var doc = element;
	
	var tagAll = [];
	if(!ukk.stringChk(tagname)) return;
	var getname = doc.getElementsByTagName(tagname);
	var len = getname.length;
	
	while(len--) tagAll[tagAll.length] = getname[tagAll.length];
	
	return tagAll;
}
ukk.$$ = function(CLS,element){
	
	if(!ukk.stringChk(CLS)) return;
	
	var tagAll = ukk.getElementsByTagNameArray('*',element);
	var len = tagAll.length;
	var cl5 =[];
	
	for (i=0;i<len;i++){
		var classes = tagAll[i].getAttribute('class') || tagAll[i].getAttribute('className');
		if(classes){
			var classnm = classes.split(' ');
			var clsArray = CLS.split(' ');
			var clsstay;
			var clearflg = 0;
			for(var z=0;z<classnm.length;z++){
			
				if(CLS.indexOf(' ') != -1 && (CLS.lastIndexOf(' ') != CLS.length-1)){			
				for(var k=0;k<clsArray.length;k++){
				
					if(clsArray[k] == classnm[z]){				
						clearflg++;
						break;
					}
				}
				if(clearflg == clsArray.length){
						cl5[cl5.length] = tagAll[i];
				}
				}else{
				
				if(CLS.lastIndexOf(' ') == CLS.length-1){
					CLS = CLS.substring(0,CLS.length-1);
				}
				
				if (CLS == classnm[z]){
					cl5[cl5.length] = tagAll[i];
				}
				}
			}
		}
	}
	
	return cl5
}

/* ブラウザ判別 */
ukk.UA = (function(){
	var doc = document;
	var ua = navigator.userAgent.toUpperCase();
	var apnm = navigator.appName.toUpperCase();
	var apver = navigator.appVersion;

	var barray = [];
	
	var mac = ua.indexOf("MAC",0) >= 0;
	var windows = ua.indexOf("WIN",0) >= 0;
	
	if(windows){
		barray['os'] = 'windows';
	}else if(mac){
		barray['os'] = 'mac';
	}else{
		barray['os'] = 'other'
	}
	
	
	if(window.attachEvent && !window.opera){
		barray['name'] = 'IE';
		
		if (typeof doc.documentElement.style.msInterpolationMode != 'undefined') {
			if(typeof document.documentMode != 'undefined') {
			barray['ver'] = '8';
			}else{
			barray['ver'] = '7';
			}
		}else{
			if(ua.indexOf('MSIE 6',0) >= 0){
				barray['ver'] = '6';
			}
		}
	}else{	

	if(ua.indexOf("SAFARI",0) >= 0 && ua.indexOf('APPLEWEBKIT/',0) >= 0){
		
			barray['name'] = 'Safari';
		
			barray['ver'] = (function(){
				var ver = ua.split("/");
				var vernum = ver[ver.length-1];
				vernum = vernum.replace(/\./g,'');
				vernum = vernum.slice(0,3);
				var n = parseInt(vernum,10)
				if (n >= 412 && n < 522){
				return '2';
				}else if (n >= 522) {
				return '3';
				}else{
				return '1';
				}
				})();
	}else if(ua.indexOf('GECKO',0) >= 0 && ua.indexOf('FIREFOX',0) >= 0){
		
		barray['name'] = 'Firefox';
		
		var ffua = ua.split('/');
		var ffver = ffua[ffua.length-1].slice(0,1);
		
		if (typeof window.postMessage != 'undefined' && ffver == '3') {
			barray['ver'] = '3';		
		}else if(ffver == '2'){
			barray['ver'] = '2';
		}else{
			barray['ver'] = '1';
		}
		
	}else if(window.opera){
		barray['name'] = 'Opera';
	}else if(ua.indexOf('NETSCAPE',0) >= 0){
		barray['name'] = 'Netscape';
		
		if(apver == '5'){
			barray['ver'] = '6';
		}
	}else{
		barray['name'] = 'unknown';
	}
	
	}
	
	return barray;
})();


ukk.Browser = function(target,version){	
	if(typeof target == 'undefined') return null;
	if(!ukk.stringChk(target)) return null;
	
	if(typeof version == 'undefined') var ver = false;
	else var ver = true; 
	
	var nm = ukk.UA.name.toUpperCase();
	var target = target.toUpperCase();
	
	if(nm.search(target) != -1) {
		
		if(ver){
			
			if(ukk.UA.ver.search(version,'i') != -1){
			return true;
			}else{
			return false;
			}		
		}
		return true;	
	}else{
		return false;
	}
} 


var boxclassoverFunc = function(){
var __boxheight = 0;
	var boxoverclass = $('contentsArea').getElementsByClassName('carBox');
	//var elems = ukk.$$(target,ukk.$(txtbox))
	var elems = $A(boxoverclass);
	elems.each(function(obj){
			
		var imgobj = obj.getElementsByTagName('img');
		
		if(imgobj.length > 1) var imgobj1 = imgobj[1];
		else var imgobj1 = imgobj[0];

		Event.observe(imgobj1,'mouseover',imgovert);
		Event.observe(imgobj1,'mouseout',imgoverf);
		Event.observe(imgobj1,'click',boxclick);	
		
		obj.observe('mouseover',boxovert);
		obj.observe('mouseout',boxoverf);

		if(__boxheight < Element.getHeight(obj)){
		__boxheight = Element.getHeight(obj);
		}

	});
	
	elems.each(function(obj){
		if(ie){
		obj.style.height = __boxheight;
		}else{
		obj.style.minHeight = __boxheight;
		}
	});

}
var boxclick = function(e){
	//Element.removeClassName(this,'boxovero');
	var imgobj = this.parentNode.parentNode;
	var boxhref = imgobj.getElementsByTagName("a")[0];
	location.href = boxhref.href;
}
var boxclick2 = function(e){
	var boxhref = this.getElementsByTagName("a")[0];
	location.href = boxhref.href;
	if (e.target) {
	e.preventDefault(); 
	}else if (window.event.srcElement) { 
	window.event.returnValue=false;
   	}
}
var imgovert = function(){
		this.style.cursor = "pointer";
}
var imgoverf = function(){
		this.style.cursor = "auto";
}
var boxovert = function(event){
		Element.addClassName(this,'boxovero');
}
var boxoverf = function(){
		Element.removeClassName(this,'boxovero');
}


DOMready.tgFunc(boxclassoverFunc);

//document.observe("dom:loaded", lineupTabover);


//document.observe("dom:loaded", test2);


})();
