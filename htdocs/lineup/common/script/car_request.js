
function CarReqData(url,imgsrc,imgalt,blocktxt){
	this.url = url;
	this.imgsrc = imgsrc;
	this.imgalt = imgalt;
	this.blocktxt = blocktxt;
}

var car_reqnavi = new Array(
new CarReqData('href="/purchase/onlinequote/index.html"','/lineup/common/images/req_bt_01.gif','オンライン見積もり','ご希望の車種のお見積もりを計算できます。'),
new CarReqData('href="/share/log/dealer_search.html"','/lineup/common/images/req_bt_02.gif','販売店検索','三菱自動車の販売店（ディーラー・サテライトショップ）を検索することができます。'),
new CarReqData('href="/share/log/demo_car.html"','/lineup/common/images/req_bt_03.gif','展示車/試乗車検索','ご試乗またはご覧になりたいクルマを取り扱う販売店（ディーラー）を検索することができます。'),
new CarReqData('href="https://customer.mitsubishi-motors.co.jp/olc/OlcTop.do\?sourceId=OLC" target="_blank"','/lineup/common/images/req_bt_04.gif','カタログ請求','ご希望の車種のカタログを請求できます。'),
new CarReqData('href="/purchase/cataloglist/index.html"','/lineup/common/images/req_bt_05.gif','カタログ一覧','ご希望の車種のカタログを、データ（PDF形式）でご覧いただけます。'),
new CarReqData('href="/reference/index.html"','/lineup/common/images/req_bt_06.gif','FAQコーナー','お問い合わせの多いご質問をＦＡＱとして掲載しております。CM情報や車名の由来などをご確認頂けます。')
);


var carfaqurl = new Array();

carfaqurl[1] = 'href="/reference/newmodelfaq/outlander/index.html"';
carfaqurl[7] = 'href="/reference/newmodelfaq/pajero/index.html"';
carfaqurl[17] = 'href="/reference/newmodelfaq/evo/index.html"' ;
carfaqurl[18] = 'href="/reference/newmodelfaq/toppo/index.html"' ;
carfaqurl[19] = 'href="/reference/newmodelfaq/galant_fortis_sportback/index.html"' ;
carfaqurl[20] = 'href="/reference/newmodelfaq/rvr/index.html"' ;


var nvnum = car_reqnavi.length;


function carReq(simunum){
if(nvnum > 0){
	
		
document.writeln('<div class="reqBlock clearfix">');	
for(var i=1;i<=nvnum;i++){
	

var reqbox = '';
var texbox = 'textBlock';
var idf = i%3;
var onclickbox = '';
var urlbox = car_reqnavi[i-1].url;
if(i==1){
//if(simunum == 20){
onclickbox = '  onkeypress="openWin(\'../common/log/onlinequote.html\',\'sim\',\'\',\'\',\'6\');return false;" onclick="openWin(\'../common/log/onlinequote.html\',\'sim\',\'\',\'\',\'6\');return false;"'
//}else{
//onclickbox = ' onClick="openSimu('+simunum+');return false;" onKeyPress="openSimu('+simunum+');return false;"'
//}
}else if(i==6){
urlbox = carfaqurl[simunum]
}



if(i==2 || i==5){
	var imgwidth=283;
}else{
	var imgwidth=284;
}
if(idf == 0){
	var classbox = ' class="rightmg_off"';
}else{
	var classbox = '';
}



reqbox += '<div'+classbox+'><h4><a '+urlbox+onclickbox+' class="imgLink"><img src="'+car_reqnavi[i-1].imgsrc+'" alt="'+car_reqnavi[i-1].imgalt+'" width="'+ imgwidth+'" height="25"><\/a><\/h4>\n';
reqbox += '<p>'+car_reqnavi[i-1].blocktxt+'<\/p><\/div>\n';
document.write(reqbox);

}
document.writeln('<\/div>');
document.writeln('<br class="clear">');
}
}