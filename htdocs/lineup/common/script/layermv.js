function htmlbaseload(){
	
	
	/////////////
	if($('mwin')) return;
	
	var url = '/lineup/common/html/layer.html';
	var baseObj = document.createElement('div');
	$('general').appendChild(baseObj);
	baseObj.id = 'mwin';
	baseObj.style.visibility = 'hidden';
	baseObj.style.display = 'none';
	var lya = new Ajax.Updater('mwin',url,{
	method:'get',
	requestHeaders: ['If-Modified-Since','Wed, 15 Nov 1995 00:00:00 GMT']
	});
	
	if($('grayfield')) return;
	var grobj = document.createElement('div');
	$('general').appendChild(grobj);
	grobj.id = 'grayfield';
	
	baseObj = null;
	delete url;
//	}
//	if(document.getElementsByClassName('flashhidden')) viewerwin.inflash = true;
}


var viewerwinMV = Class.create();
viewerwinMV.prototype = {

initialize: function (ctnum,carname,category,fchk,inpg){
//this.start=(new Date()).getTime();
if(!$('grayfield')) return;
if(!$('mwin')) return;
	this.inpg = inpg;
	this.ctnum = ctnum;
	this.carname = carname;
	this.category = category;
	
//	if(this.carname == 'outlander'){
//	return;
//	}
	
	this.items_width = 0;
	this.items_height = 0;
	this.gfield = $('grayfield');
	this.mwin = $('mwin');
	if(!this.inpg || this.inpg != 'undefined'){
	this.getwindowsize();
	}
	
	
	this.fchk = fchk;
	this.stopflag = false;
	var mvXML;
	$('popupmv').style.display = 'block';
	
	this.imgsPath = '/'+this.carname+'/common/popupswf/';
	this.imgnumber = this.ctnum.split('_');
	this.filecategory = this.imgnumber[0];
	this.imgnow = Number(this.imgnumber[1]);
	
	this.categorynm = this.category.split('_');
	this.pgcategorynm = this.categorynm[1];
	this.filecategory = this.categorynm[0];
	this.imgfield = $('imgfield');
	this.bigimg = $('popupmv');
	$('bigimg').style.display = 'none';
	
	this.clickchk = true;
	
	if(!viewerwinMV.fstflg[this.fchk]){
	return this.nextfunc(0);
	}else{
	return this.nextfunc(1);
	}
},

nextfunc:function(nxflg){
	this.flashhidden();

	if ( this.stopflag ) return;
 	var func = function(){
		//ref.stopflag = true;
		//clearTimeout(time);
		if(nxflg == 0){
        this.loadXMLFile();
		}else{
		this.secLoad();	
		}
    };
	var time = setTimeout( func.bind(this), 10 );
},

flashhidden: function(){
	this.flvobj = document.getElementsByClassName('flashhidden');
	this.flvobj.each(function(obj){
		if(obj.style.visibility == 'hidden') obj.style.visibility = 'visible';
		else obj.style.visibility = 'hidden';
	});
},

loadXMLFile: function(){

	if(this.fchk == 1 || this.fchk == 4 || this.fchk == 5){

	var showcomp = function(httpObj) {
	mvXML = httpObj.responseXML;
	this.secLoad().bind(this);
	};
	
	var reqxmlName = '/' + this.carname + '/common/xml/movieset_'+this.filecategory+'.xml';
	new Ajax.Request(reqxmlName,{
	method:'get',
	onFailure:  function(httpObj) {
	alert('読み込みエラーが発生しました。ページをリロードしてください。');
	},
	onComplete: showcomp.bind(this)
	});
	
	}
},

secLoad: function(){
	
	this.lycloseListener = this.lycloase.bindAsEventListener(this);
	this.scrollListener = this.lypositionSC.bindAsEventListener(this);
	this.resizeListener = this.lypositionSC.bindAsEventListener(this);

	Event.observe(window, 'scroll', this.scrollListener);
	Event.observe(window, 'resize', this.resizeListener);
	
	Event.observe($('viewerclose'), 'click', this.lycloseListener);
	Event.observe(this.mwin, 'click', this.lycloseListener);
	Event.observe(this.gfield, 'click', this.lycloseListener);
		
	viewerwinMV.fstflg[this.fchk] = true;

		var items = mvXML.getElementsByTagName(this.category)[0];
		var items_imgset = items.getElementsByTagName('mvset');
		var itemswh = mvXML.getElementsByTagName('mvgroup')[0];
	
		var imgnum = this.imgnumber[1] -1;
	
		var filenm = items_imgset[imgnum].getElementsByTagName('mvurl')[0];
		var swfPath = this.imgsPath + filenm.getAttribute('mvname');
		//var swfPath = filenm.getAttribute('mvname');
			
		if(items_imgset[imgnum].getElementsByTagName('maintxt')){
				var resutxtheight = 0;
				var resutxt = $('resutxt');

				var txtnm = items_imgset[imgnum].getElementsByTagName('maintxt');
				
				var txtleng = txtnm.length;
				var plist = '';
				
					for(var j=0; j<txtleng; j++){
						var txtoption = txtnm[j].getAttribute('option');
						
						if(txtoption == 'notes') var cssoption = ' class = "notes"';		
						else if(txtoption == 'bold') var cssoption = ' class = "bold"';
						else var cssoption = '';	
						
						plist += '<p'+cssoption+'>'+txtnm[j].firstChild.nodeValue + '<\/p>';
					}
				Element.update(resutxt,plist);
				resutxt.style.display = 'block';
		}
	
		this.imgfield.style.display = 'block';
		this.mwin.style.display = 'block';

		this.mwin.style.width = Number(itemswh.getAttribute('width')) + 'px';
		this.mwin.style.height = Number(itemswh.getAttribute('height')) + resutxt.offsetHeight + 'px';

	
	//	this.mwin.style.width = Number(itemswh.getAttribute('width')) + 'px';
	//	this.mwin.style.height = Number(itemswh.getAttribute('height')) + resutxt.offsetHeight + 'px';
		this.getwindowcenter(true);
	
	
	
	var so = new SWFObject(swfPath, "carFlash", itemswh.getAttribute('mvwidth'), itemswh.getAttribute('mvheight'), "7", "#ffffff");
	so.write("popupmv");
	
},




lycloase: function(){
	this.gfield.style.display = 'none';
	this.inpg = false;
	Event.stopObserving(this.gfield, 'click', this.lycloseListener);
	Event.stopObserving(this.mwin, 'click', this.lycloseListener);
	Event.stopObserving($('viewerclose'), 'click', this.lycloseListener);
	Event.stopObserving(window, 'scroll', this.scrollListener);
	Event.stopObserving(window, 'resize', this.resizeListener);
	
	this.flashhidden();
	this.imgreload();
},


lypositionSC: function(){
//var position = Element.getStyle(this.gfield, 'position');
this.getwindowsize();
this.getwindowcenter(false);
},


getwindowsize: function() {
var bwidth=document.body.clientWidth;
if(!safari){
var bheight=document.body.clientHeight;
if(ie || IE7) this.gfield.style.top = document.body.scrollTop;
}else{
var bheight = document.body.scrollHeight;
}
this.gfield.style.width  = bwidth + 'px';
this.gfield.style.height = bheight + 'px';
this.gfield.style.display = 'block';
},

getwindowcenter: function(sc) {
 var sizeObj = Element.getDimensions(this.mwin);
 var sctop = 0;
 var winscr = 0;

 	if (window.innerWidth && window.innerHeight) { 
	
 	var bwidth=window.innerWidth;
 	var bheight=window.innerHeight;
	
	winscr = 15;
	sctop = document.body.scrollTop;
	
	}else if ( document.documentElement && document.documentElement.clientWidth != 0 ){
		
	var bwidth=document.documentElement.clientWidth;
	var bheight=document.documentElement.clientHeight;
	
	winscr = 15;
	sctop = document.documentElement.scrollTop;
	
	}else if ( document.body ) {

	var bwidth=document.body.clientWidth;
	var bheight=document.body.clientHeight;
	
	winscr = 15;
	sctop = document.body.scrollTop;
	} 
	
    this.mwin.style.left  = (bwidth/2) - (sizeObj.width/2) + winscr + 'px';
	if(sc){
   	this.mwin.style.top = (bheight/2) - (sizeObj.height/2) + sctop + 'px';
	this.mwin.style.visibility = 'visible';
	}
},


imgreload: function(){

	Element.setStyle(this.mwin,{'display':'none','visibility':'hidden'});
	//Element.setStyle(this.gfield,{'width':'1px','height':'1px'});	
	this.imgfield.style.display = 'none';
	$('resutxt').style.display = 'none';
	$('resutxt').innerHTML = '';
	//$('imgnv').style.display = 'none';
	this.bigimg.innerHTML = '';
	
}

}
viewerwinMV.fstflg = new Array();
viewerwinMV.count = 0.5;


Event.observe(window, 'load', htmlbaseload);
