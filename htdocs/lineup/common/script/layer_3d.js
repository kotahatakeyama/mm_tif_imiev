
var viewBox = {};


viewBox.Imgsrc = {

	add : function (objsrc,plusnm){
		if(objsrc.indexOf(plusnm) != -1) return objsrc;
		var ftype = objsrc.substring(objsrc.lastIndexOf('.'), objsrc.length);
		var presrc = objsrc.replace(ftype, plusnm + ftype);
		return presrc;
	},
	
	del : function (objsrc,plusnm){
		if(objsrc.indexOf(plusnm) == -1) return objsrc;
		var ftype = objsrc.substring(objsrc.lastIndexOf('.'), objsrc.length);
		var presrc = objsrc.replace(plusnm + ftype, ftype);
		return presrc;
	}
}
//* viewerコントロール *//

var viewerControl =Class.create({

	//* 初期設定 *//
	initialize: function(maxcount,grade){
		
		this.defgrade = grade;
		this.nowgrade = this.defgrade;
		this.defcl = viewBox.defcl;
		this.nowcl = viewBox.zcl;
		this.count = viewBox.count;
		this.maxcount = maxcount;
		this.timeout;
		
		this.baseArea = $('viewerBase');
		this.inAreaMousePos = 0;
		this.dragFlag = false;
		
		this.nowCar = $(this.defgrade + '_' + this.defcl);
		this.nowPos = 0;
		this.xw =0;
		this.loadingBox = $('loadingBox');
		this.grChg = $('gradeChg');
		this.cllink = $('colorChg').getElementsByTagName('img');
		
		this.btn = $('nextBtn');
		
	},
	
	unloadfunc : function(){
		this.baseArea = null;
		this.nowCar = null;
		this.loadingBox = null;
		this.grChg = null;
		this.cllink = null;
		this.btn = null;
		viewerfunc.defaultset = null;
		viewerfunc.gradeset = null;
	},
	
	//* ロード完了後実行 *//
	locomp : function(){
		
		this.defcl = viewBox.defcl;
		this.count = viewBox.count;
		this.defgrade = viewBox.defgr;
		viewBox.cllist[this.defgrade].each(function(obj){
													
			if(ie) $('cl_' + obj).style.filter = 'alpha(opacity=100)';
			else $('cl_' + obj).style.opacity = 1;							
			//Element.setStyle($('cl_' + obj),{'opacity':'1'});
			$('cl_' + obj).className='';
							
		});
				
		if($(this.defgrade + '_' + this.defcl))	{
	
			this.nowCar = $(this.defgrade + '_' + this.defcl);
		}else{
	
			Position.clone($('cl_' + viewBox.zcl).getElementsByTagName('img')[0], viewBox.btstay);
			this.nowCar = $(this.defgrade + '_' + viewBox.zcl);
			this.defcl = viewBox.zcl;
		}

		viewBox.btstay.style.visibility = 'visible';
		this.countSet();
		
		$('cltx').innerHTML = $('cl_' + viewBox.zcl).getElementsByTagName('img')[0].title;
		
		var pre = this;
		//this.nowCar.style.display = 'block';
		this.nowCar.style.visibility = 'visible';
		//if(viewBox.cache[this.nowgrade] == null){
		
		
		
	
	//	new Effect.Appear(this.loadingBox, {
      //          from: 1.0,
     //     	    to: 0.0,
     //           duration: 0.5,
	//			afterFinishInternal: function(effect) {
				pre.loadingBox.style.display = 'none';
	//			}
	//	});
		//}
		this.grChg.style.visibility = 'visible';


		viewBox.loadingend = true;
		
	},
	
	//* 各ボタンイベント設定 *//
	eventSet : function(){
		
		this.mousedgls = this.mousedg.bindAsEventListener(this);
        this.mousemvdgls = this.mousemvdg.bindAsEventListener(this);
		this.mouseupdgls = this.mouseupdg.bindAsEventListener(this);
		
		Event.observe(this.baseArea, 'mousedown', this.mousedgls);
		Event.observe(this.baseArea, 'mousemove', this.mousemvdgls);
		Event.observe(document, 'mouseup', this.mouseupdgls);
		
		this.turnLls = this.turnL.bindAsEventListener(this);
        this.turnRls = this.turnR.bindAsEventListener(this);
		this.imgStopls = this.imgStop.bindAsEventListener(this);
		
		var turnXL = document.getElementById('turnX_Left');
		var turnXR = document.getElementById('turnX_Right');
		
		Event.observe(turnXL, 'mousedown', this.turnLls);
		Event.observe(turnXR, 'mousedown', this.turnRls);
		
		Event.observe(turnXL, 'mouseover', this.imgOverFunc);
		Event.observe(turnXR, 'mouseover', this.imgOverFunc);
		
		Event.observe(turnXR, 'mouseout', this.imgStopls);
		Event.observe(turnXL, 'mouseout', this.imgStopls);
		
		Event.observe(turnXR, 'mouseup', this.imgStopls);
		Event.observe(turnXL, 'mouseup', this.imgStopls);
		
		Event.observe(turnXR, 'mouseout', this.imgOutFunc);
		Event.observe(turnXL, 'mouseout', this.imgOutFunc);
				
		Event.observe(document, 'mouseup', this.imgStopls);

		this.clChgFuncls = this.clChgFunc.bindAsEventListener(this);
	//	this.cloverFuncls = this.cloverFunc.bindAsEventListener(this);
				
		var cllen = this.cllink.length;
		var x=0;
				
		for(;x<cllen;++x){
			Event.observe(this.cllink[x], 'click', this.clChgFuncls);
		}

		this.gradeChg = this.gradeChgFunc.bindAsEventListener(this);
		Event.observe(this.grChg, 'change', this.gradeChg);
		
		this.docone = this.doconeFunc.bindAsEventListener(this);
		Event.observe(this.btn, 'click', this.docone);
		
		viewBox.eventFlag = true;
		
		this.unloadfuncls = this.unloadfunc.bindAsEventListener(this);
		Event.observe(window, 'unload', this.unloadfuncls);
		
		this.locomp();
	},
	
	eventDelete : function(){	 
		 
		Event.stopObserving(this.baseArea, 'mousedown', this.mousedgls);
		Event.stopObserving(this.baseArea, 'mousemove', this.mousemvdgls);
		Event.stopObserving(document, 'mouseup', this.mouseupdgls);
	//	Event.observe(this.baseArea, 'mouseout', this.mouseupdgls);
				
		var turnXL = document.getElementById('turnX_Left');
		var turnXR = document.getElementById('turnX_Right');
		
		Event.stopObserving(turnXL, 'mousedown', this.turnLls);
		Event.stopObserving(turnXR, 'mousedown', this.turnRls);
		//Event.stopObserving(turnXL, 'mousedown', this.imgOverFunc);
		//Event.stopObserving(turnXR, 'mousedown', this.imgOverFunc);
		Event.stopObserving(turnXL, 'mouseover', this.imgOverFunc);
		Event.stopObserving(turnXR, 'mouseover', this.imgOverFunc);
		
		Event.stopObserving(turnXR, 'mouseup', this.imgStopls);
		Event.stopObserving(turnXL, 'mouseup', this.imgStopls);
		
		Event.stopObserving(turnXR, 'mouseout', this.imgStopls);
		Event.stopObserving(turnXL, 'mouseout', this.imgStopls);
		//Event.stopObserving(turnXR, 'mouseup', this.imgOutFunc);
		//Event.stopObserving(turnXL, 'mouseup', this.imgOutFunc);
		Event.stopObserving(turnXR, 'mouseout', this.imgOutFunc);
		Event.stopObserving(turnXL, 'mouseout', this.imgOutFunc);
		
		//Event.observe(turnXR, 'mouseover', this.imgover);
		//Event.observe(turnXL, 'mouseover', this.imgover);
		
		Event.stopObserving(document, 'mouseup', this.imgStopls);

	//	this.cloverFuncls = this.cloverFunc.bindAsEventListener(this);
				
		var cllen = this.cllink.length;
		var x=0;
				
		for(;x<cllen;++x){
			Event.stopObserving(this.cllink[x], 'click', this.clChgFuncls);
			//Event.stopObserving(this.cllink[x], 'mouseover', this.cloverFuncls);
		//	Event.stopObserving(this.cllink[x], 'mouseout', this.cloverFuncls);
			
		}

		Event.stopObserving(this.grChg, 'change', this.gradeChg);
		Event.stopObserving(this.btn, 'click', this.docone);
	//	viewBox.cache = [null];
	},
	
	//* 右回転 *//
	turnR : function(e){
			
		if(!viewBox.loadingend) return;
		stopbubble(e);
		var pre = this;

		var loop = function(){
			if(pre.count < pre.maxcount-1) pre.count++;
			else pre.count=0;
			pre.nowCar.style.left = -660 * pre.count + 'px';
			pre.imgtime = setTimeout(loop,50);
		}
		this.imgtime = setTimeout(loop,50);
		
		
	},
	
	//* 左回転 *//
	turnL : function(e){
		if(!viewBox.loadingend) return;
		stopbubble(e);
		var pre = this;

		var loop = function(){
			if(pre.count <= 0) pre.count =pre. maxcount-1;
			else pre.count--;
	
			if(pre.nowCar.style.left >= 0) pre.nowCar.style.left = -660 * (pre.maxcount-1) + 'px';
			pre.nowCar.style.left = -660 * pre.count + 'px';
				
			pre.imgtime = setTimeout(loop,50);
		}
		this.imgtime = setTimeout(loop,50);
	},
	
	//* 回転ストップ *//
	imgStop : function(e){	
		clearTimeout(this.imgtime);	
		stopbubble(e);	
	},
	
	imgOverFunc : function (e){
		Event.element(e).src = viewBox.Imgsrc.add(Event.element(e).src, '_a');
	},
	imgOutFunc : function (e){
		Event.element(e).src = viewBox.Imgsrc.del(Event.element(e).src, '_a');
	},
	
	//* ドラッグ処理 *//
	mousemvdg : function(e){
		if(!viewBox.loadingend) return;
				stopbubble(e);
				
				if(this.dragFlag){
					
					var offset =this.baseAreaPos();
									
					var offsetX = Event.pointerX(e) - offset[0];
					this.xw += offsetX - this.inAreaMousePos;
					
					if(this.xw < -22){
					if(this.count <= 0) this.count = this.maxcount-1;
					else this.count--;
					
					if(this.nowCar.style.left == 0) this.nowCar.style.left = -660 * (this.maxcount-1);
					this.nowCar.style.left = -660 * this.count + 'px';					
					
					this.xw =0;
					
					}
					if(this.xw > 22){
						if(this.count < this.maxcount-1) this.count++;
						else this.count=0;
						this.nowCar.style.left = -660 * this.count + 'px';
						this.xw =0;
					}
					
					this.inAreaMousePos = offsetX;
				}

	},
	
	//* グレード変更 *//
	gradeChgFunc : function(e){
		
		if(!viewBox.loadingend) return;
		
		var pregrade = this.nowgrade;
		
		this.nowgrade = Event.element(e).value;
		viewBox.defgr = this.nowgrade;
		viewBox.defcl = this.nowcl;
		viewBox.count = this.count;
		
		
		viewBox.zcl = this.nowcl;
			
		this.loadingBox.style.display = 'block';
		viewBox.loadingend = false;
		viewBox.loadingNum =0;
		//this.baseArea.innerHTML = '';
		this.eventDelete();
		
		if(viewBox.carName == 'PAJERO'){
		
		if(this.nowgrade == 'LONG') viewBox.zcl = 'BLKGRAY';
		else viewBox.zcl = 'SILVER';
		
		}
		
		var flagcl = false; 
		viewBox.cllist[this.nowgrade]
		
		for(var k=0;k<viewBox.cllist[this.nowgrade].length;++k){
			if(viewBox.cllist[this.nowgrade][k].indexOf(viewBox.zcl) == -1){
				flagcl = true;
			}else{
				flagcl = false;
				break;
			}
		}

		if(flagcl) viewBox.zcl = viewBox.cllist[viewBox.defgr][1];

			
		
		location.href = "indexb.html?TYPE=ALL-gra_0" + Event.element(e).selectedIndex +"&COLOR=" + viewBox.zcl + "&POSITION=" + viewBox.count;
		
		//var viewer = new viewer360(viewerfunc.gradeset[viewerfunc.loaded[this.nowgrade]]);
		//viewer.imgpreload();
		
	},
	
	//* カラー変更 *//
	clChgFunc : function(e){
		
		if(!viewBox.loadingend) return;
		
		var pn = Event.element(e).parentNode;
		
		if(pn.className.indexOf('nocolor') != -1) return;
		
		Position.clone(Event.element(e), viewBox.btstay)
		viewBox.btstay.style.visibility = 'visible';
		
		$('cltx').innerHTML = Event.element(e).title;
		//$(this.nowgrade + '_' + this.nowcl).style.display ='none';
		this.nowcl = pn.id.split('_')[1];
		//this.nowCar = $(this.nowgrade + '_' + this.nowcl);
		//viewBox.defcl = this.nowcl;

			viewBox.zcl = this.nowcl;
			viewBox.count = this.count;
			
			this.loadingBox.style.display = 'block';
			viewBox.loadingend = false;
			viewBox.loadingNum =0;
	
			//var viewer = new viewer360(viewerfunc.gradeset[viewerfunc.loaded[this.nowgrade]]);
			viewBox.cont.imgpreload();
	
	},
	
	//* ベースエリア基準値取得 *//
	baseAreaPos : function(){
		var offset = Position.cumulativeOffset(this.baseArea);
		return offset;
	},
	
	//* mouseup時 *//
	mouseupdg : function(){
		this.xw =0;
		this.dragFlag = false;
	},
	
	//* 初期ouseDown位置 *//
	mousedg : function(e){
		var b = this.baseAreaPos();
		this.inAreaMousePos = Event.pointerX(e) - b[0];
		this.dragFlag = true;
		stopbubble(e);
	},
	
	//* 移動計算 *//
	countSet : function(){	
		this.nowCar.style.left = -660 * this.count + 'px';
	},
	
	doconeFunc : function(){
		
	/*scd=RVR&g=E&ccd=WHITE&nm=&imgcd=02&kd=2wd*/
	if(!viewBox.loadingend) return;
	
	
	if(this.count < 9) var count_s = '0' + (this.count+1);
	else var count_s = this.count +1;
	
	
	
	openWin('/lineup/docone/'+viewBox.carName.toLowerCase() +'.html?scd='+viewBox.carName+'&g=' +this.nowgrade +'&ccd='+ viewBox.zcl +'&imgcd='+count_s,'popdocone');
	
	}
	
});


var viewer360 = Class.create({
	
	initialize: function(gradeset){

		this.grade = gradeset;
		this.gradeName = gradeset.getAttribute('type');
		this.gradeCut = parseInt(gradeset.getAttribute('view'),10);
		
		var preImg = '';
		var z=0;
		
		//this.cloneSpan = document.createDocumentFragment();
	//	
		//while(this.gradeCut > z){
		//	var span = document.createElement('span');
		//	this.cloneSpan.appendChild(span);
			//var span = document.createElement('img');
			//this.cloneSpan.appendChild(span);
		//	++z;
		//}
		
		this.colorSet = gradeset.getElementsByTagName('color');
		this.imgcount = 0;
		this.imglng = [];
		//this.imglng[this.colorSet.length-1] =;
		this.base = $('viewerBase');
		this.loadingBox = $('loadingBox');
		
		this.div2 = document.createElement('div');
		var divst2 = this.div2.style;
		divst2.width = 660 * this.gradeCut + 'px';
		divst2.visibility = 'hidden';
		this.base.appendChild(this.div2);
		this.img = document.createElement('img');
		this.div2.appendChild(this.img);
		//this.j=0;
		this.nexBtn = $('nextBtn');
		this.gradec = $('gradeChg');
	
		
		this.unloadfuncls = this.unloadfunc.bindAsEventListener(this);
		Event.observe(window, 'unload', this.unloadfuncls);
	},
	
	unloadfunc : function(){
		this.loadingBox = null;
		this.img = null;
		this.loadimg = null;
		this.div2 = null;
		this.base = null;
	},
	
	imgpreload : function(){

		this.areaLock();
		$('gradeChg').selectedIndex = viewBox.mm;
	//	if(ie) this.loadingBox.style.filter = 'alpha(opacity=100)';
	//	else this.loadingBox.style.opacity = 1;	
		
		var type = '.jpg';
		
		this.clname = viewBox.zcl;
		//var div = this.div2.cloneNode(true);
		this.div2.id = this.gradeName +'_'+ this.clname;
			
		if(viewBox.carName == 'RVR' || viewBox.carName == 'i-MiEV'){
		var imgp = 'view_images/'+this.gradeName + '/' + viewBox.carName + '_' + this.gradeName + '_' + this.clname + type;
		}else{
		
	//	var imgp = 'view_images/'+ this.gradeName + '/' + viewBox.carName + '_' + this.gradeName + '_' + this.clname + type;
		var imgp = 'http://www.mitsubishi-motors.com/3d_viewer/view_images/'+viewBox.carName+'/' + viewBox.carName + '_' + this.gradeName + '_' + this.clname + type;
		}
		//if(ie) var cache = '?' +Math.floor(Math.random()*100);
		//else var cache =  '';
		
		//this.img.src = '/lineup/common/images/ajax-loader.gif';
		this.loadimg = null;
		this.loadimg = new Image();
		//this.loadimg.src = imgp + cache;

		this.loadimg.src = imgp;
			
		this.img.src= this.loadimg.src;
			
		
				
		this.loadingimg();
		this.cont = '';
	},
	
	loadingimg : function(){
          if (this.loadimg.complete) {
                this.imgrelease();
            }else{
                var pre = this;
                if (navigator.userAgent.indexOf("Safari") != -1) {
                    this.safariOnLoad = function(){
                        if (pre.loadimg.complete) {
                            clearTimeout(timer);
                            pre.imgrelease();
                        }else{
                            var timer = setTimeout(pre.safariOnLoad, 100);
                        };
                    };
                    this.safariOnLoad();
                }else{
                    Event.observe(this.loadimg, 'load', this.imgrelease.bind(this), false);
                }
            }
	
	},
	
	areaLock : function(){
		
		if(ie) this.nexBtn.style.filter = 'alpha(opacity=50)';
		else this.nexBtn.style.opacity = 0.5;
		
		this.gradec.disabled = true;
		viewBox.loadingend = false;
	},
	
	areaUnLock : function(){
		if(ie) this.nexBtn.style.filter = 'alpha(opacity=100)';
		else this.nexBtn.style.opacity = 1;
		
		this.gradec.disabled = false;
		viewBox.loadingend = true;
	},
	
	imgrelease : function(){

	//this.imglng[viewBox.loadingNum]++;
	//if(this.colorSet.length > this.j){
		
	this.cle = document.getElementById('cl_'+this.clname);	
	if(this.cle){
		viewBox.cllist[this.gradeName].push(this.clname);
		Element.setStyle(this.cle,{'opacity':'1'});
		if(ie) this.cle.style.filter = 'alpha(opacity=100)';
		else this.cle.style.opacity = 1;
		this.cle.className="";
	}
	//++this.j	
	//this.time = setTimeout(this.imgpreload.bind(this),0);
	//}else{
	//clearTimeout(this.time)	
	//}
	//if(this.imglng[viewBox.loadingNum] >= this.gradeCut){
		//this.imgcount=1;
		this.areaUnLock();
		if(!viewBox.eventFlag){
			viewBox.cont2 = new viewerControl(this.gradeCut,this.gradeName);
			viewBox.cont2.eventSet();
		}
		//if(viewBox.loadingNum ==0){
			
			viewBox.defgr = this.gradeName;
			viewBox.cont2.locomp();
			
		//}
		
		//viewBox.loadingNum++;

	//}
			
	/*if(viewBox.loadingNum == 1){
		if(!viewBox.eventFlag){
			viewBox.cont = new viewerControl(this.gradeCut,this.gradeName);
			viewBox.cont.eventSet();
		}else{
			viewBox.defgr = this.gradeName;
			viewBox.cont.locomp();
		}
	}
	
	if(viewBox.loadingNum >= this.colorSet.length){
	//	viewBox.loadingNum=0;
	//
	console.log(viewBox.loadingNum);
		viewBox.defgr = this.gradeName;
		viewBox.cont.locomp();
		this.areaUnLock();
		
		
	}*/
	
	}
});


var viewerfunc = {
	
	grpos :0,
	
	loaded : [],
	
	fstloading : function(){
	viewBox.cllist = [];
	//viewBox.carName = 'RVR';
	viewBox.loadingNum = 0;
	viewBox.count = 1;
	//viewBox.defgr = 'G';
	//viewBox.defcl = 'KAWASEMI';
	viewBox.eventFlag = false;
	viewBox.cont = '';
	viewBox.cont2 = '';
	viewBox.cache = [null];
	viewBox.loadingend = false;
		
	
	viewBox.btstay = document.createElement('img');
	viewBox.btstay.src = 'images/cl_bt_stay.gif';
	viewBox.btstay.id = 'btStay';

	$('contArea').appendChild(viewBox.btstay);
	
	
	viewBox.clli = $("colorChg").getElementsByTagName('li');
	
	var cllilng = viewBox.clli.length;
	var d=0;
	
	while(d<cllilng){
		viewBox.clli[d].className = 'nocolor';
		Element.setStyle(viewBox.clli[d],{'opacity':'0.1'});
		if(ie) viewBox.clli[d].style.filter = 'alpha(opacity=10)';
		else viewBox.clli[d].style.opacity = 0.1;
		d++
	}
	
	
	
//	
		new Ajax.Request('xml/gradeset_360b.xml', {
					method: 'get',
					onFailure: function(httpObj){
						alert('読み込みエラーが発生しました。ページをリロードしてください。');
					},
					onComplete: function(httpObj){
						var XML = httpObj.responseXML;
						viewerfunc.defaultset = XML.getElementsByTagName('defaultSetting');
						viewerfunc.gradeset = XML.getElementsByTagName('grade');
						
						viewerfunc.glng = viewerfunc.gradeset.length;
						
						viewerfunc.gradeloading();
						delete XML; 
					}
		});
	
	},
	
	gradeloading : function(){
		
		$A(viewerfunc.gradeset).each(function(obj,index){
		
		viewerfunc.loaded[obj.getAttribute('type')] =  parseInt(index,10);
		viewBox.cllist[obj.getAttribute('type')] = [];
		
		var dd = obj.getElementsByTagName('color');

		$A(dd).each(function(obj2){
														
		viewBox.cllist[obj.getAttribute('type')].push(obj2.getAttribute('type'));
		
		});
		
		
		
		});
		
		
		viewBox.carName = (viewerfunc.defaultset[0].getAttribute('name'));
		viewBox.defcl = viewerfunc.defaultset[0].getAttribute('color');
		viewBox.defgr = viewerfunc.defaultset[0].getAttribute('type');
		viewBox.zcl = viewBox.defcl;
		Position.clone($('cl_' + viewBox.zcl).getElementsByTagName('img')[0], viewBox.btstay);
		
		
		var path = location.search;
		if(path.indexOf('?') != -1) {
		var grnm = path.substring(1);
			
		var pre = this;
		this.ddm = [];
		
		grnm.split('&').each(function(obj){
			
			var box = obj.split('=');
			//pre.ddm[box[0]] = box[1];
			//alert(pre.ddm[box[0]]);
			
			if(box[0] == 'TYPE') viewBox.carGrade = box[1].split('-');
			if(box[0] == 'COLOR') viewBox.zcl =  box[1]
			if(box[0] == 'POSITION') viewBox.count =  parseInt(box[1],10);
		})
		}else{
		viewBox.carGrade = 'ALL'
		}
		
		
		if(viewBox.carGrade == 'ALL'){
			viewBox.mm = 0;
			viewBox.cont = new viewer360(viewerfunc.gradeset[viewerfunc.loaded[viewBox.defgr]]);
		}else{
		
		if(viewBox.carGrade[0] != 'ALL' ) $('gradeSelectArea').style.display = 'none';
		
		var gradeNum = viewBox.carGrade[1].split('_');
		viewBox.mm = parseInt(gradeNum[1],10);
		viewBox.defgr = viewerfunc.gradeset[viewBox.mm].getAttribute('type');
		
		viewBox.cont = new viewer360(viewerfunc.gradeset[viewBox.mm]);
		}
		
		viewBox.cont.imgpreload();

	}
}

//DOMready.tgFunc(viewerfunc.fstloading);





