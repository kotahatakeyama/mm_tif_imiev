

var specviewer2 = Class.create();
specviewer2.prototype = {

initialize: function (pgname,carname,eqname,eqwidth,widthcolspan){
	var alltime = (new Date).getTime();
	this.loadtxt = new String();
	
	this.pgname = pgname;
	this.carname = carname;
	this.eqname = eqname;
	this.eqwidth = eqwidth;
	this.widthcolspan = widthcolspan;

	this.itembox = '';
	this.items = '';

	this.catenum = 1;
	this.stopflag = false;
	this.loadXMLFile();
},

loadXMLFile: function(){
	
	var showcomp = function(httpObj) {
	var XML = httpObj.responseXML;
	this.items = XML.getElementsByTagName('specgroup');
	this.tableset();
	};
	var xmlurl = '/' + this.carname + '/common/xml/equip_'+this.pgname+'.xml?'+(new Date).getTime();
	new Ajax.Request(xmlurl,{
	
	method:'get',
	
	onFailure:  function(httpObj) {
	$('base01loadimg').innerHTML = '読み込みエラーが発生しました。ページをリロードしてください。<br>またはxmlが存在しないか、読み込めません。';
	},
	
	onComplete: showcomp.bind(this)
	});

},

nextfunc:function(num){

	
  if ( this.stopflag ) return;
    
    var func = function(){
       
	   this.tableset(num);
		clearTimeout(time);
    };
   var time = setTimeout( func.bind(this), 10 );

},

tableset: function(catenum){
//タグ取得
var categorySet = this.items[0].getElementsByTagName('category');
if(this.catenum<categorySet.length){
	
var k=this.catenum;
var notes_box ='';
var classnm = '';
var mainitem = '';
var colspan_num = '';
var subtxt = '';

var rowcount =0;
var equipnm = new Array();
var equip;
var getequip;
var eflag = true;

var rowcount2 =0;
var equipnm2 = new Array();
var equip2;
var getequip2;
var eflag2 = true;

var maxnum = 1;
var colflg = new Array();

var equipSet = categorySet[k].getElementsByTagName('equip');
var colSet = equipSet[0].getElementsByTagName('col');

	var loadimg = document.getElementById(this.pgname + 'loadimg');
	var imgParent = loadimg.parentNode;
//var baseObj = document.createElement('div');
//baseObj.id = 'specbaseset'+k;
//$('tableoutput'+k).appendChild(baseObj);

//類別rowspan処理
for(var i=0; i<equipSet.length-1;i++){

var equipbox = equipSet[i].getElementsByTagName('col');
subequip = equipbox[0].firstChild.nodeValue;
	if(eflag){
		if(equipSet[i].getAttribute('name') == equipSet[i+1].getAttribute('name')){
		equip = equipSet[i].getAttribute('name') ;
		getequip = i;
		equipnm[i] = rowcount +1;
		equipnm[i+1] = 0;
		eflag = false;
		if(subequip != '☆'){
		colflg[i] = true;	
		}
		}
	}else{
		if(equip == equipSet[i].getAttribute('name')){
		equipnm[getequip] += 1;
		if(subequip != '☆'){
		colflg[getequip] = true;
		}else if(colflg[getequip]){
		colflg[i] = true;
		}
		equipnm[i] = 0;
		}else{
		i--;
		eflag = true;
		}
	}
}

//価格rowspan処理

for(var i=0; i<equipSet.length-1;i++){

var remarkbox = equipSet[i].getElementsByTagName('remark');
var remarknum = remarkbox[0].firstChild.nodeValue;

var remarkbox2 = equipSet[i+1].getElementsByTagName('remark');
var remarknum2 = remarkbox2[0].firstChild.nodeValue;


	if(eflag2){
		if(remarknum == remarknum2 && remarknum != 'n'){
		equip2 = remarknum;
		getequip2 = i;
		equipnm2[i] = rowcount2 +1;
		equipnm2[i+1] = 0;
		eflag2 = false;
		}

	}else{
		if(equip2 == remarknum && remarknum !='n'){
		equipnm2[getequip2] += 1;
		equipnm2[i] = 0;
		}else{
		i--;
		eflag2 = true;
		}
	}
}



	

//類別colspan処理
for(var i=1; i<equipSet.length;i++){
var equipbox = equipSet[i].getElementsByTagName('col');
subequip = equipbox[0].firstChild.nodeValue;
	if(subequip == '☆'){
	baseeqcolspan = '';
	}else{
	baseeqcolspan = ' colspan="2"';
	break;
	}
}

var equipSet = categorySet[k].getElementsByTagName('equip');
var colSet = equipSet[0].getElementsByTagName('col');

var tabletag = '<table width="740" border="0" cellpadding="0" cellspacing="0" class="spec tbscroll">\n';

if(baseeqcolspan == ''){
tabletag +=  '<col width="'+this.eqwidth[0]+'">';
var spbox = '<img src="/share/images/spacer.gif" alt="" height="1" width="'+this.eqwidth[0]+'">'
}else{
tabletag +=  '<col width="'+this.widthcolspan[0]+'"><col width="'+this.widthcolspan[1]+'">';
var spbox = '<img src="/share/images/spacer.gif" alt="" height="1" width="'+this.widthcolspan[0]+'">'
var spbox2 = '<img src="/share/images/spacer.gif" alt="" height="1" width="'+this.widthcolspan[1]+'">'
}

for(var i=1;i<=colSet.length;i++){
tabletag +=  '<col width="'+this.eqwidth[i]+'">';
}


//装備カテゴリー行
if(baseeqcolspan == ''){
tabletag += '<thead><tr class="mainheader bkspecgray2"><th class="tdleft" colspan="'+colSet.length+'"><span>'+this.eqname[k-1]+'<\/span><\/th>\n';
}else{
tabletag += '<thead><tr class="mainheader bkspecgray2"><th class="tdleft" colspan="'+(colSet.length+1)+'"><span>'+this.eqname[k-1]+'<\/span><\/th>\n';
}

	if(k>1){
	tabletag += '<td class="bordernone tdright"><span><a href="#" onClick="$(\'tbload\').scrollTop = \'0\';return false;"><img src="/lineup/images/carcmn_im_05.gif" class="top" alt="上へ戻る"><\/a><\/span><\/td><\/tr>\n';
	}else{
		tabletag += '<td class="bordernone">&nbsp;<\/td><\/tr>\n';
	}

if(baseeqcolspan == ''){
tabletag += '<tr class="mainheader bkspecgray2 spline"><th class="tdleft"'+baseeqcolspan+'>'+spbox+'<\/th>\n';
}else{
tabletag += '<tr class="mainheader bkspecgray2 spline"><th class="tdleft">'+spbox+'<\/th><td class="bordernone">'+spbox2+'<\/td>\n';
	
}
for(var i=1;i<=colSet.length;i++){
	spbox = '<img src="/share/images/spacer.gif" alt="" height="1" width="'+this.eqwidth[i]+'">'
	if(i>=colSet.length && k>1){
	tabletag += '<td class="bordernone tdright">'+spbox+'<\/td>\n';
	}else{
		tabletag += '<td class="bordernone">'+spbox+'<\/td>\n';
	}
}
tabletag += '<\/tr><\/thead><tbody>'

//各要素書き出し
for(var i=0; i<equipSet.length;i++){
var equipbox = equipSet[i].getElementsByTagName('col');
subequip = equipbox[0].firstChild.nodeValue;
	if(subequip.indexOf('☆') != -1 && baseeqcolspan != ''){
	eqcolspan = ' colspan="2"';
	}else if(subequip.indexOf('☆') != -1 && baseeqcolspan == ''){
	eqcolspan = '';
	}else{
	eqcolspan = '';
	}

var colnumber = equipbox.length;

tabletag += '<tr>\n';

var equipitem = equipSet[i].getAttribute('name');

//先頭、途中の※置き換え

var lrg_notes = equipitem;
	
	if(equipitem.indexOf('※') != -1){
	var lrgbox = lrg_notes.match(/※[1-9]{1,2}/g);
	lrg_notes = '<\/span><span class="notes_num">'+ lrgbox+'<\/span><br><span>';
	}else{
	lrg_notes = '';
	}
equipitem=equipitem.replace(lrgbox,lrg_notes);

//装備名br置き換え
equipitem=equipitem.replace(/brem/g,"b_rem");
equipitem=equipitem.replace(/br/g,"<br>");
equipitem=equipitem.replace(/b_rem/g,"brem");

/* ※検索 */
var notes_notes = '';
	if(equipSet[i].getAttribute('notes')){
		if(equipSet[i].getAttribute('notes').indexOf('_') != -1){
		var equipnotes = equipSet[i].getAttribute('notes').split('_');

		for(var n=0;n<equipnotes.length;n++){
		notes_notes += '※' + equipnotes[n] + '&nbsp;';
		}
		}else{
		notes_notes = '※' + equipSet[i].getAttribute('notes');
		}
	notes_box = '<\/span> <span class="notes_num">'+ notes_notes;
	}else{
	notes_box ='';
	}

var subequip = equipbox[0].firstChild.nodeValue;
var specerbox = ''; 

if(subequip.indexOf('☆') != -1 && baseeqcolspan != ''){
eqcolspan = ' colspan="2"';
}else if(subequip.indexOf('☆') != -1 && baseeqcolspan == ''){
eqcolspan = '';
}else{
eqcolspan = '';
}

if(equipitem.indexOf('◆') != -1){
var cocochimk = ' class="cocochifont"';
}else{
var cocochimk = '';
}

var equipnmbox = equipnm[i];
if(equipnmbox >= 2){
var rowspannm = ' rowspan="'+ equipnmbox +'"';
//tabletag += '<th class="tdleft"'+ rowspannm + eqcolspan+' width="'+this.widthcolspan[0]+'"><span>'+equipitem+notes_box+'</span></th>\n';
tabletag += '<th class="tdleft"'+ rowspannm + eqcolspan+'><span'+cocochimk+'>'+equipitem+notes_box+'</span></th>\n';
}else if(equipnmbox == 0){
}else{
tabletag += '<th class="tdleft"'+eqcolspan+' width="'+this.eqwidth[0]+'"><span'+cocochimk+'>'+equipitem+notes_box+'</span></th>\n';
//tabletag += '<th class="tdleft"'+eqcolspan+'"><span>'+equipitem+notes_box+'</span></th>\n';
}


var colspanflg = false;

for(var j=0; j<colnumber;j++){
	
mainitem = equipbox[j].firstChild.nodeValue;

	if(!colspanflg){
/* ※検索 */
	var notes_notes = '';
		if(equipbox[j].getAttribute('notes')){
			if(equipbox[j].getAttribute('notes').indexOf('_') != -1){
			var notesnum = equipbox[j].getAttribute('notes').split('_');
				for(var x=0;x<notesnum.length;x++){
				notes_notes += '※' + notesnum[x] + ' ';
				}
			}else{
			notes_notes = '※' + equipbox[j].getAttribute('notes');
		}
		notes_box = '</span><span class="notes_num">'+ notes_notes;
	}else{
	notes_box ='';
	}


/* span検索 */
if(equipbox[j].getAttribute('span')){
colspan_num = ' colspan="'+equipbox[j].getAttribute('span')+ '"';
colspanflg = true;
}else{
colspan_num = '';
colspanflg = false;
}

if(mainitem.indexOf(' ') != -1 || mainitem == 'n'){
mainitem = '&nbsp;'
classnm = '';
}else{
var lrg_notes = mainitem;
mainitem=mainitem.replace(/br/g,"<br>");
	if(mainitem.indexOf('※') != -1){
	var lrgbox = lrg_notes.match(/※[1-9]{1,2}/);
	lrg_notes = '</span><span class="notes_num">'+ lrgbox+'</span> ';
	}else{
	lrg_notes = '';
	}
	if(mainitem.indexOf('●') != -1 || (mainitem != 'n' && mainitem.indexOf('△') == -1 && mainitem.indexOf('OP') == -1 && mainitem.indexOf('★') == -1)){
	classnm = ' class="bklb"';
		if(mainitem.length >1){
		mainitem=mainitem.replace("●","●<br>");
		}
	}else if(mainitem.indexOf('△') != -1) {
	classnm = ' class="bkspecDOP"';
	}else if(mainitem.indexOf('OP') != -1) {
	classnm = ' class="bkore"';
	}else if(mainitem.indexOf('★') != -1) {
	classnm = ' class="bkspecRE"';
		if(mainitem.length >1){
		mainitem=mainitem.replace("★","★<br>");
		}
	}else if(mainitem.indexOf('◎') != -1) {
	classnm = ' class="bkspecRTE"';
	}else if(mainitem.indexOf('▲') != -1) {
	classnm = ' class="bkspecSOP"';
	}else{
	classnm = '';
	}
mainitem=mainitem.replace(lrgbox,lrg_notes);
}

if(subequip.indexOf('☆') != -1 && j==0 && !colflg[i]){
}else if(j==0){
//tabletag += '<td class="tdleft"><span>'+mainitem+subtxt+'</span>'+notes_box+'</td>\n';	
tabletag += '<td class="tdleft" width="'+this.widthcolspan[1]+'"><span>'+mainitem+subtxt+'</span>'+notes_box+'</td>\n';	

}else if(classnm == '' && notes_box){
tabletag += '<td'+colspan_num+'><span>'+mainitem+subtxt+'</span>'+notes_box+'</td>\n';	
}else{
tabletag += '<td'+classnm+colspan_num+'><span>'+mainitem+subtxt+'</span>'+notes_box+'</td>\n';	
}
}else{
	colspanflg = false;
}
}



var remarkbox = equipSet[i].getElementsByTagName('remark');
mainitem = remarkbox[0].firstChild.nodeValue;

/* ※検索 */
var notes_notes = '';
if(remarkbox[0].getAttribute('notes')){
	if(remarkbox[0].getAttribute('notes').indexOf('_') != -1){
	var remarknum = remarkbox[0].getAttribute('notes').split('_');

		for(var n=0;n<remarknum.length;n++){
		notes_notes += '※' + remarknum[n] + '&nbsp;';
		}
	}else{
	notes_notes = '※' + remarkbox[0].getAttribute('notes');
	}
notes_box = '</span><span class="notes_num">'+ notes_notes;
}else{
notes_box ='';
}

if(mainitem.indexOf(' ') != -1 || mainitem == 'n'){
mainitem = '&nbsp;'
}else{
var lrg_notes = mainitem;
mainitem=mainitem.replace(/br/g,"<br>");
	if(mainitem.indexOf('※') != -1){
	var lrgbox = lrg_notes.match(/※[1-9]{1,2}/);
	lrg_notes = '</span><span class="notes_num">' + lrgbox+'</span><br><span class="notes">';
	}else{
	lrg_notes = '';
	}
mainitem=mainitem.replace(lrgbox,lrg_notes);
}


var equipnmbox2 = equipnm2[i];
var eqwidthleng = this.eqwidth.length-1;

if(equipnmbox2 >= 2){
	
var equipbox = equipSet[i].getElementsByTagName('col');
subequip = equipbox[0].firstChild.nodeValue;
if(subequip == '☆'){
var rowspannm2 = ' rowspan="'+ equipnmbox2 +'"';
}else{
var rowspannm2 = ' rowspan="'+ (equipnmbox2) +'"';
}

	
//tabletag += '<td><span>'+mainitem + notes_box+'</span></td>\n';
tabletag += '<td'+rowspannm2+' width="'+this.eqwidth[eqwidthleng]+'"><span>'+mainitem + notes_box+'</span></td>\n';
}else if(equipnmbox2 == 0){
}else{
//tabletag += '<td><span>'+mainitem + notes_box+'</span></td>\n';
tabletag += '<td width="'+this.eqwidth[eqwidthleng]+'"><span>'+mainitem + notes_box+'</span></td>\n';
}



tabletag += '</tr>\n';
}
tabletag += '<\/tbody></table>';

var element = document.createElement('div'); 
element.id = ('tableoutput'+k); 
element.innerHTML = tabletag;

imgParent.insertBefore(element, loadimg);

//$(this.pgname + 'Box').appendChild(element);

if(k==categorySet.length-1) {
imgParent.removeChild(loadimg);
}

//$('tableoutput'+k+this.swch).innerHTML = tabletag;
//document.getElementsByClassName('loadtb').style.display = 'none';
}


if(k==categorySet.length){
//this.stopflag = false;
this.stopflag = true;
}else{

this.catenum++;
this.nextfunc(this.catenum);
}

}

}

/////
/////

var standardviewer = Class.create();
standardviewer.prototype = {

initialize: function (myid,xmlid,grid){
	this.myid = myid;
	this.xmlid = xmlid;
	this.grid = grid;
	var stnditems = new Array();
	if(standardviewer.standardxmlfstflg){
		this.carname = '';
		this.items = "";
		this.loadXMLFile();
		standardviewer.standardxmlfstflg = false;
	}else{
		this.equipstn();
	}
},

loadXMLFile: function(){
	var xmlurl = '../common/xml/equip_standard'+this.xmlid+'.xml?'+(new Date).getTime();

	var showcomp = function(httpObj) {
	var XML = httpObj.responseXML;
	stnditems = XML.getElementsByTagName('specgroup');
	XML = null;
	httpObj = null;
	this.equipstn();
	};
	new Ajax.Request(xmlurl,{
	
	method:'get',
	
	onFailure:  function(httpObj) {
	$('tableoutput').innerHTML = '読み込みエラーが発生しました。ページをリロードしてください。<br>またはxmlが存在しないか、読み込めません。';
	},
	
	onComplete: showcomp.bind(this)
	});

},

equipstn: function(){

if(this.myid == 'allflag') {
var def = 1;
var boxlength = 7;
}else{
var idnum = this.myid.match(/[0-9]/g);
var def = idnum;
var boxlength = idnum;
}

if(!standardviewer.standardxmlfstflg){
	var gradenum = stnditems[0].getElementsByTagName('equipStandard');
	var gradeequip = gradenum[this.grid-1].getElementsByTagName('equip_category');
}

	for(var h=def;h<=boxlength;h++){
		
	var equipbox = gradeequip[h-1].getElementsByTagName('equip');

	var targetid = "Equip" +h;	
	var lihtml;
	var tagchk = document.getElementById(targetid).getElementsByTagName('li');
	if(tagchk.length<1) {
	
	var grobjul = document.createElement('ul');
	
	for(var m=0;m<equipbox.length;m++){
		
		var grobjli = document.createElement('li');
		grobjul.appendChild(grobjli);
		
		var grobjp = document.createElement('p');
		grobjli.appendChild(grobjp);
	
		var mainitem = equipbox[m].firstChild.nodeValue;
	
		var notes_notes = '';
		if(equipbox[m].getAttribute('notes')){
		
		if(equipbox[m].getAttribute('notes').indexOf('_') != -1){
		var notesnum = equipbox[m].getAttribute('notes').split('_');
			for(var n=0;n<notesnum.length;n++){
			notes_notes += '※' + notesnum[n] + '&nbsp;';
			}
		}else{
		
		notes_notes = '※' + equipbox[m].getAttribute('notes');
		}

		notes_box = '</span>&nbsp;<span class="notes_num">'+ notes_notes;

		}else{
		notes_box ='';
		}
		
		var lrg_notes = mainitem;
		if(mainitem.indexOf('※') != -1){
		var lrgbox = lrg_notes.match(/※[1-9]{1,2}/);
		lrg_notes = '</span><span class="notes_num">' + lrgbox+'</span>&nbsp;<span>';
		}else{
		lrg_notes = '';
		}
		
		mainitem=mainitem.replace(lrgbox,lrg_notes);
		mainitem=mainitem.replace(/brem/g,"b_rem");
		mainitem=mainitem.replace(/br/g,"<br>");
		mainitem=mainitem.replace(/b_rem/g,"brem");
		
		if(mainitem.indexOf('◆') != -1){
		var cocochimk = ' class="cocochifont"';
		}else{
		var cocochimk = '';
		}
		
		grobjp.innerHTML = '<span'+cocochimk+'>' + mainitem + notes_box;

	}
	$(targetid).appendChild(grobjul);
	}
if(document.getElementById('stnload'+h)){
	var loadimg = document.getElementById('stnload'+h);
	var imgParent = loadimg.parentNode;
	imgParent.removeChild(loadimg);
}
}
}
}
standardviewer.standardxmlfstflg = true;


var sconoff = {
btflg : true,
fstflg : true,	

uplink : function() {
	if(this.btflg) $('tbload').scrollTop = '0'
	else $('general').scrollTop = '0'
},


specfld : function(flg){
var tbld = $('tbload');
var moo = tbld.getElementsByTagName('div');

if(flg == '0') {
	tbld.style.height = 'auto'; 
	
	for(var tt=1;tt<moo.length;tt++){
	if(this.fstflg){
	var tbhcopy = $('tbh').cloneNode(true);
	tbhcopy.id = 'copyheader' + tt;
	tbhcopy.setAttribute('summary','');
	tbhcopy.style.borderBottom = 'none';
	var dd = moo[tt].parentNode;
	dd.insertBefore(tbhcopy,moo[tt]);
	}
	$('copyheader' + tt).style.display = 'block';
	$('copyheader' + tt).style.marginTop = '30px';
	
	}
	this.fstflg = false;
	this.btflg = false;
} else {
	tbld.style.height = '400px';
	if(!this.fstflg){
	for(var tt=1;tt<moo.length;tt++){
		$('copyheader' + tt).style.display = "none";
		$('copyheader' + tt).style.marginTop = '0px'; 
	}
	this.btflg = true;
	}
}
} 

}


function specline (){
	if(localnv_sys.classnm.indexOf('outlander')  == -1) return;

	var tbld = $('tbload');
	var moo = tbld.getElementsByTagName('table');

	
	var basebox = document.createElement('span');
	basebox.style.backgroundColor = '#ef0041';
	basebox.style.width = '888px';
	basebox.style.height = '2px';
	basebox.id = "specline";
	basebox.style.position = "absolute";
	basebox.style.top = "0";
	basebox.style.left = "0";
	basebox.style.display = 'none';
	basebox.innerHTML = "<img src='/share/images/spacer.gif'>";
	
	$('general').style.position = 'relative';

	tbld.appendChild(basebox);

//	$('tbload').appendChild(basebox2);
	for(var v=0;v<moo.length;v++){
		
	Event.observe(moo[v], 'mouseover', function(event){
											 
	var mouseY = Event.pointerY(event);
	var sctop = $('tbload').scrollTop;
	basebox.style.top = mouseY - $('tbload').offsetTop + sctop -5;
	
	basebox.style.display = 'block';
	},false);
	
	Event.observe(moo[v], 'mouseout', function(event){
											 
	basebox.style.display = 'none';
	},false);
	}
	
	
	
}

addListener(window, 'load', specline);
