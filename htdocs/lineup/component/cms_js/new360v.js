$(function ready(){


  function new360v_frames(frames){
    var frames= 36, every= 1, stack= []
    for(var i= 1; i <= frames; i+= every){
      var name= [i, '.png'].join('')
      while (name.length < 7) name= '0'+name
      stack.push(name)
    }
    return stack
  }


  prepare_reel_vrview= function(target){
    var
      $vrview= $(target).parent('.vrview'),
			opts= $('pre', $vrview).text(),
			options= eval('('+opts+')')

		$vrview.click(function(e){
			if (!$(this).is('.on')){
				click_it(e);
				return false;
			}
		});
		$('h3', $vrview).click(click_it);

    function click_it(e){
      var
        onoff= !$vrview.hasClass('on'),
				$others= $('.vrview').not($vrview)
			if (!e.altKey){
				$others.removeClass('on');
	      $('.jquery-reel', $others).trigger('teardown');
			}

      $vrview[onoff ? 'addClass' : 'removeClass']('on');
      onoff && $(target).reel(options) || $(target).trigger('teardown');
			$.cookie('new360v.web.vrview', onoff ? $(target).selector : null);
			// $.cookie(onoff ? '')
			return false;
    }
  }

});