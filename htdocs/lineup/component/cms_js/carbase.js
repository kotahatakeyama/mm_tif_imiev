
var module = {};
if(!$.support.cssFloat){
if (typeof document.documentElement.style.msInterpolationMode == "undefined") module.oldIE = true;
if(typeof document.documentMode>=8) module.IE8 = true;
module.IE = true;
}

$(function(){

	
	$('div.carContentBlockInner','#carContentWrap').each(function(){
		objLine($(this).find('div.boxTtlInner').get());
	});
  
    $('div.carContentBlockInner','#carContentWrap').each(function(){
		objLine($(this).find('div.txLine').get());
	});

	localNav_stay();
	
	$('img.imgover').hover(function(){
		this.src = imgsrcover.add(this.src,'_a');
	},function(){
		if(this.className.indexOf('stay') == -1) this.src = imgsrcover.del(this.src,'_a');
	});
	
	
	$('div.layoutBlock3_top,div.layoutBlock4_top').find('div.box').hover(function(){
		
	
		$(this).find('a.overlay').attr('href',$(this).find('div.boxInner a').attr('href'));
		$(this).find('a.overlay').attr('target',$(this).find('div.boxInner a').attr('target'));
		
		$(this).find('a.overlay').css({'height':$(this).height()-3,'opacity':0.2,'display':'block'});
		
	},
	function(){
		
	
			$(this).find('a.overlay').css({'display':'none'});

		
		
	}).click(function(){
		
	
	
		
	}).append('<a class="overlay" style="display:none;" href="#"></a>');
	
	
	
	$('div.index_speBox').parent().hover(function(){
		
	$(this).css({'cursor':'pointer'});
		
	//	$(this).find('a.overlay').css({'height':$(this).height(),'opacity':0,'display':'block'});
		
	},
	function(){
		
	
	//	$(this).find('a.overlay').css({'display':'none'});

		
		
	}).click(function(e){
		var target = $(this).find('div.txBox a').attr('target');
		
		if(target == '_blank'){
			window.open($(this).find('div.txBox a').attr('href'));
		}else{
			location.href = $(this).find('div.txBox a').attr('href');
		}
		
		stopbubble(e);

		
	})//.append('<a class="overlay" style="display:none;" href="#"></a>');
	
	var tr = $('#grsel').find('tr');
	$('#grselBtn').find('a').click(function(e){
		
		stopbubble(e);
		
		$(this).parent().parent().find('.stay').removeClass('stay');
		$(this).parent().addClass('stay');
		
		$('tr.checker').removeClass('checker');
		
		
		var num = this.href.split('#')[1].match(/\d{1,3}/g);
		
		$.each(num,function(index){
			
		$(tr[num[index]]).addClass('checker');
		
			
		});
		
		
	});
	
	
	$('tr.clickPoint').find('td').hover(function(){
		
	$(this).css({'cursor':'pointer'});
		
	},
	function(){
	}).click(function(e){
		location.href = $(this).parent().find('a.cilink').attr('href');
	});
	
	$('.clickBlock').hover(function(){
		
	$(this).css({'cursor':'pointer'});
		
	},
	function(){
	}).click(function(e){
		var target = $(this).find('a.clickBlockURL').attr('target');
		
		if(target == '_blank'){
			window.open($(this).find('a.clickBlockURL').attr('href'));
		}else{
			location.href = $(this).find('a.clickBlockURL').attr('href');
		}
	});
	
});




function stopbubble(e){	
	if (e.target) { 
	e.stopPropagation(); 
	e.preventDefault(); 
	}else if (window.event.srcElement) { 
	window.event.cancelBubble = true; 
	window.event.returnValue=false;
	}
}

var objLine = function(domElm){
	var ddlng = domElm.length;
	
	if(module.IE && module.oldIE) var stset = "height";
	else var stset = "minHeight";
	
	//if(ddlng <= 1) return;
	var zz = 0;
	for(var k=0;k<ddlng;++k){
			//if(ie && !IE7) domElm[k].style.height = 0;
			//else domElm[k].style.minHeight = 0;
			if(zz <= domElm[k].offsetHeight){
			var gotHeight = domElm[k].offsetHeight;
			var myStyle = domElm[k].currentStyle || document.defaultView.getComputedStyle(domElm[k],"");
			if(myStyle.paddingTop){var padTop = Number(myStyle.paddingTop.replace("px",""));}
			if(myStyle.paddingBottom){var padBtm = Number(myStyle.paddingBottom.replace("px",""));}
			if(myStyle.borderTopWidth){
				if(myStyle.borderTopWidth.match(/[0-9]/) !== null){
					bdTop = Number(myStyle.borderTopWidth.replace("px",""));
				}else{
					bdTop = 0;
				}
			}
			if(myStyle.borderBottomWidth){
				if(myStyle.borderBottomWidth.match(/[0-9]/) !== null){
					bdBtm = Number(myStyle.borderBottomWidth.replace("px",""));
				}else{
					bdBtm = 0;
				}
			}

			zz = (gotHeight - padTop - padBtm - bdTop - bdBtm);
				
		}
	}
	
	for(var k=0;k<ddlng;++k){
	  if(domElm[k].className == 'boxTtlInner'){  
		  domElm[k].parentNode.style.height = zz + 'px';
	  }else{
		  domElm[k].style[stset] = zz + 'px';
	  }
	}
}


var imgsrcover = {
	add : function (objsrc,plusnm){
		if(objsrc.indexOf(plusnm) != -1) return objsrc;
		var ftype = objsrc.substring(objsrc.lastIndexOf('.'), objsrc.length);
		var presrc = objsrc.replace(ftype, plusnm + ftype);
		return presrc;
	},
	del : function (objsrc,plusnm){
		if(objsrc.indexOf(plusnm) == -1) return objsrc;
		var ftype = objsrc.substring(objsrc.lastIndexOf('.'), objsrc.length);
		var presrc = objsrc.replace(plusnm + ftype, ftype);
		return presrc;
	}
}
var localNav_stay =  function(){
	
	if(!document.getElementById('carlocal_menu')) return;
	
	var bc = $('body').attr('class');
	$('#carlocal_menu').find('li').each(function(){
	if(this.id == bc){
		var src = imgsrcover.add($(this).find('img').attr('src'),'_a');
		$(this).find('img').attr('src',src).addClass('stay');
		
		
		
	}
		
	});
	
}


