// =========================================================
//
//	common.js
//	全ページ共通で利用するjavascriptを記載
//
// =========================================================

// ===============================================
//
//	グローバル変数
// -----------------------------------------------
var mmc = {},
    sp_path = '/sp/';

// ===============================================
//
//	初期化
// -----------------------------------------------
mmc.window = {
	elm : $(window),
	width : 0,
	height : 0,
	size_get : {}
};
mmc.head = {
	title : '',
	pathname : location.pathname,
	alternate : $('link[rel="canonical"]').attr('href')
};
mmc.body = {
	elm : {},
	page : {},
	contents : {}
};
mmc.overlay = {
	opened : false,
	opened_name : '',
	moving : false,
	close : null
};
mmc.lightbox = {
	opened : false
};
mmc.ua = {
	base : navigator.userAgent,
	os : ''
};
if(mmc.ua.base.indexOf("iPhone") > -1 || mmc.ua.base.indexOf("iPod") > -1){
	mmc.ua.os = 'ios';
} else if(mmc.ua.base.indexOf("Android") > -1) {
	if(mmc.ua.base.indexOf("Android 2") > -1){
		mmc.ua.os = 'old_android';
	} else {
		mmc.ua.os = 'android';
	}
} else {
	mmc.ua.os = 'others';
}

// ===============================================
//
//	ウインドウのサイズを取得
// -----------------------------------------------
mmc.window.size_get = function(){
	mmc.window.width = Math.floor(mmc.window.elm.width());
	mmc.window.height = Math.floor(mmc.window.elm.height());
	//console.log(mmc.window.width, mmc.window.height);
};

// ===============================================
//
//	ヘッダメニュー関連
// -----------------------------------------------
var header_navi = function(){
	var elm = {
		menu : $('#GlobalMenu'),
		btn : {
			search : $('#GlobalMenu #GlobalMenuSearch'),
			clip : $('#GlobalMenu #GlobalMenuClip'),
			menu : $('#GlobalMenu #GlobalMenuSitemap')
		},
		inside_seach : $('#InsideSearch'),
		clip : $('#Clip'),
		sitemap : $('#Sitemap')
	};
	
	// 要素を外部ファイルから追加
	elm.inside_seach.load(sp_path + 'inc/inside_search.inc');
	elm.clip.load(sp_path + 'inc/clip.inc', function(){
		my_clip();
	});
	elm.sitemap.load(sp_path + 'inc/sitemap.inc');

	// 展開動作を実装
	elm.btn.search.overlay_sct({
		elm : elm.inside_seach,
		name : 'GlobalMenuSearch'
	});
	elm.btn.clip.overlay_sct({
		elm : elm.clip,
		name : 'GlobalMenuClip'
	});
	elm.btn.menu.overlay_sct({
		elm : elm.sitemap,
		name : 'GlobalMenuSitemap'
	});
};

// ===============================================
//
//	オーバーレイ・セクション
// -----------------------------------------------
$.fn.overlay_sct = function(option){
	var opt = $.extend({
			elm : {},
			btn : $(this),
			name : '',
			height : 0
		}, option),
		head = $('#GlobalHeader');
	
	var open_move = function(option){
		var op = $.extend({
			state : true
		}, option);

		mmc.window.elm.scrollTop(0);
		mmc.overlay.close = opt.elm;

		if( $('.inner', opt.elm).innerHeight() > mmc.window.height ){
			opt.height = $('.inner', opt.elm).innerHeight();
		} else {
			opt.height = mmc.window.height;
		}
		opt.btn.addClass('opened');

		opt.elm.stop().animate({
			height : '100%'
		}, 400, function(){
			mmc.body.contents.hide();
			mmc.overlay.opened = true;
			mmc.overlay.opened_name = opt.name;
			mmc.overlay.moving = false;
		}).addClass('opened');

		if( mmc.ua.os != 'old_android' && op.state === true ){
			history.replaceState('overlay_' + opt.name + '_close', null);
			history.pushState('overlay_' + opt.name + '_open', null);
		}
	};
	
	var close_move = function(element, callback){
		mmc.body.contents.show();
		element.stop().animate({
			height : 0
		}, 400, function(){
			$('li.opened', head).removeClass('opened');
			mmc.overlay.opened = false;
			mmc.overlay.moving = false;
			if( callback ){
				callback();
			}
		}).removeClass('opened');
	};
	
	$(this).click(function(){
		if( mmc.overlay.moving === true || mmc.lightbox.opened === true ){
			return;
		}
		mmc.overlay.moving = true;
		if( mmc.overlay.opened === false ){
			head.addClass('overlay_open');
			open_move();
		} else {
			if( $(this).attr('id') == mmc.overlay.opened_name ){
				if( mmc.ua.os != 'old_android' ){
					history.back();
				} else {
					close_move(opt.elm, function(){
						head.removeClass('overlay_open');
					});
				}
			} else {
				close_move($('.overlay.opened'), function(){
					open_move({
						state : false
					});
				});
			}
		}
	});
	
	$(opt.elm).on('click', '.close', function(){
		if( mmc.overlay.moving === true ){
			return;
		}
		mmc.overlay.moving = true;
		if( mmc.ua.os != 'old_android' ){
			history.back();
		} else {
			close_move(opt.elm, function(){
				head.removeClass('overlay_open');
			});
		}
	});

	window.addEventListener('popstate', function(e){
		if( e.state == 'overlay_' + opt.name + '_open' ){
			head.addClass('overlay_open');
			open_move({
				state : false
			});
		}
		if( e.state == 'overlay_' + opt.name + '_close' && mmc.overlay.close ){
			close_move(mmc.overlay.close, function(){
				head.removeClass('overlay_open');
			});
		}
	}, false);
};


// ===============================================
//
//	イベント設定・実行
// -----------------------------------------------
$(document).ready(function(){
	mmc.body.elm = $('body');
	mmc.body.page = $('#Page');
	mmc.body.contents = $('#Contents');
	mmc.head.title = $('#ArticleHeader h1').html();
	if( mmc.head.title == undefined && mmc.head.pathname == '/sp/' ){
		mmc.head.title = 'トップページ';
	}

	mmc.window.elm.on('resize.win_size_get', function(){
		mmc.window.size_get();
	});
	
	mmc.window.elm.on('orientationchange.win_size_get', function(){
		mmc.window.size_get();
	});
	
	mmc.window.size_get();

	mmc.body.elm.on('click.link_in_overlay', '.overlay a:not([target="_blank"])', function() {
		if( mmc.ua.os == 'old_android' ){
			return;
		}
		var href = $(this).attr('href');
		if( mmc.head.pathname == href ){
			history.go(-1);
		} else {
			location.replace(href);
		}
		return false;
	});

	header_navi();
});