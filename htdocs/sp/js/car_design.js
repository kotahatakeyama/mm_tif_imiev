// =========================================================
//
//	car_design.js
//	車種デザイン/カラーページのためのjs
//
// =========================================================
// ===============================================
//
//	イベント設定・実行
// -----------------------------------------------
$(document).ready(function(){
	$(".image_gallery").fadeSlide();
	$('.color_variation').color_variation();
  $('.color_variation.only_color').each(function(){
    $(this).color_chip_select();
  });
});