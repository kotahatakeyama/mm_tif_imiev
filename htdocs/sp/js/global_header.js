// =========================================================
//
//	global_header.js
//	共通ヘッダの呼び出し用
//
// =========================================================
var load_header = function(option){
	var opt = $.extend({
			index : false
		}, option),
		header_html = '';

	if( opt.index === false ){
		header_html += '<div id="MitsubishiLogo"><a href="/sp/"><img src="/sp/images/common/lg.png" alt="Drive@earth MITSUBISHI MOTORS" width="57" height="46"></a></div>';
	} else {
		header_html += '<h1 id="MitsubishiLogo"><img src="/sp/images/common/lg.png" alt="Drive@earth MITSUBISHI MOTORS" width="57" height="46"></h1>';
	}

	header_html += '<div id="GlobalMenu">';
	header_html += '<ul>';
	header_html += '<li id="GlobalMenuSearch">検索</li>';
	header_html += '<li id="GlobalMenuClip">クリップ</li>';

	//if( opt.index === false ){
		header_html += '<li id="GlobalMenuSitemap">MENU</li>';
	//}

	header_html += '</ul>';
	header_html += '</div><!-- / #GlobalMenu -->';
	
	$(document).ready(function(){
		$('#GlobalHeader').html( header_html );
		header_navi();
	});
};
