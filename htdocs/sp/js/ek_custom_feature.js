
$(function(){
	if( $('.box-slide-down').length ) {
		$('.btn-slide-down').click(function(){
			var $box = $(this).closest('.box-slide-down');
			var $cnt = $box.find('.cnt-slide-down')
			$cnt.stop(true, false);
			if(!$box.hasClass('opened')) {
				$box.addClass('opened');
				$cnt.slideDown(400);
			} else {
				$box.removeClass('opened');
				$cnt.slideUp(400);
			}
			return false;
		});
	}
});