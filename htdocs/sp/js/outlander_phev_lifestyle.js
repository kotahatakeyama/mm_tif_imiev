$(function(){
    var profileFhow = false;
    $("#Lifestyle.episode .mv > a").click(function(){
        $("html, body").animate({scrollTop : 0}, 200);
        $("#profile").css({minHeight : $(window).outerHeight()}).fadeIn(200);
        $("#Page").css({height: $("#profile .content").outerHeight(), overflow : "hidden"});
        $("#profile").fadeIn(200);
        profileFhow = true;
        return false;
   });
   $("#profile .content > a").click(function(){
       $("#profile").fadeOut(200);
        $("#Page").css({height: "auto", overflow : "visible"});
        profileFhow = false;
       return false;
   });
   $(window).resize(function(){
       if(profileFhow){
        $("#profile").css({minHeight : $(window).outerHeight()});
        $("#Page").height($("#profile .content").outerHeight());
       }
   });
});