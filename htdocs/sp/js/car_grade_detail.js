// =========================================================
//
//	car_grade_detail.js
//	車種・グレード詳細ページのためのjs
//
// =========================================================
var tab_section = function(){
	var elm = {
			navi : $('.tab_section .tab_navi li'),
			table : $('.tab_section')
		},
		table_arr = [];

	$('.spec_table', elm.table).each(function(indx){
		if( indx > 0 ){
			$(this).hide();
		} else {
			$(this).addClass('show');
		}
	});

	$('a', elm.navi).each(function(indx){
		table_arr[indx] = $( $(this).attr('href') );
		
		$(this).on('click', function(){
			if( $(this).hasClass('current') ){
				return false;
			}
			$('a.current', elm.navi).removeClass('current');
			$(this).addClass('current');
			$('.show', elm.table).removeClass('show').hide();
			table_arr[indx].show().addClass('show');
			return false;
		});
	});
};

// ===============================================
//
//	イベント設定・実行
// -----------------------------------------------
$(document).ready(function(){
	$("#MainImage").flipSlide({
		next:".next",
		prev:".preview"
	});
	$('#BodyColor').color_chip_select();
	tab_section();
	$('.clean_energy').lightbox({
		elm : $('#CleanEnergy')
	});
});