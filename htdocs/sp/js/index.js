// =========================================================
//
//	index.js
//	トップページ用のjs
//
// =========================================================
var topics_index_load = function(callback){
	var topics_html = '';
	$.ajax({
		url: '/sp/csv/topics_index.csv',
		type: 'GET',
		success: function(data) {
			var csv = $.csv()(data);

			for( var i = 0; i < csv.length; i++){
				var indx = i;
				topics_html += '<div class="item"><a href="' + csv[indx][1] + '"><img src="' + csv[indx][2] + '" alt="' + csv[indx][0] + '"></a></div>';
			}

			$('.topics_slide .banner .inner').prepend(topics_html);
			callback();
		},
		error: function() {
			return;
		}
	});
};
// ===============================================
//
//	イベント設定・実行
// -----------------------------------------------
$(document).ready(function(){
	information();
	$("#MainImage").flipSlide({
		next:".next",
		prev:".preview"
	});
	topics_index_load(function(){
		$("#Topics").flipSlide({
			viewport : ".banner",
			transitionFlag:false,
			next:".next",
			prev:".preview"
		});
	});
	
});