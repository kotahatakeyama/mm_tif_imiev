// =========================================================
//
//	global_footer.js
//	共通フッタの呼び出し用
//
// =========================================================
var load_footer = function(option){
	var elm = {},
		support_html = '',
		share_html = '',
		topics_html = '',
		alternate_html = '',
		others_html = '',
		opt = $.extend({
			support_visible : true,
			support_catalog : 'https://customer.mitsubishi-motors.co.jp/olc/OlcTop.do?sourceId=OLC',
			support_estimate : 'http://try.mitsubishi-motors.co.jp/olm/EGS0001.do',
			support_search : 'http://democar.mitsubishi-motors.co.jp/democar/carSelect.html',
			support_dealership : 'http://map.mitsubishi-motors.co.jp/',
			share : true,
			topics_visible : true,
			topics_csv_path : '/sp/csv/topics_common.csv',
			alternate_visible : true,
			alternate_text : '',
			alternate_url : 'http://www.mitsubishi-motors.co.jp/'
		}, option);

	$(document).ready(function(){
		elm.footer = $('#GlobalFooter');

		if( opt.support_visible === true ){
			elm.footer.append('<div id="Support" />');
			support_html += '<section>';
			support_html += '<h1>ご購入サポート</h1>';
			support_html += '<ul>';
			support_html += '<li class="catalog"><a href="' + opt.support_catalog + '" class="pc" target="_blank">カタログを請求する</a></li>';
			support_html += '<li class="estimate"><a href="' + opt.support_estimate + '" target="_blank">Webで見積もる</a></li>';
			support_html += '<li class="dealership"><a href="' + opt.support_dealership + '" class="pc" target="_blank">販売店を検索する</a></li>';
			support_html += '<li class="search"><a href="' + opt.support_search + '" class="pc" target="_blank">試乗車/展示車を探す</a></li>';
			// support_html += '<li class="inquiry"><a href="/sp/reference/">お問い合わせ<br>(お客様相談センター)</a></li>';
			support_html += '</ul>';
			support_html += '</section>';
			$('#Support', elm.footer).html(support_html);
		}

		if( opt.share === true ){
			elm.footer.append('<div id="Share" />');
			share_html += '<section>';
			share_html += '<h1>情報を送る/シェアする</h1>';
			share_html += '<ul>';
			share_html += '<li class="mail"><a href="mailto:?body=' + document.title + ' ' + document.URL + '"><span>メールで送る</span></a></li>';
			share_html += '<li class="facebook"><a href="http://www.facebook.com/share.php?u=' + document.URL + '" target="_blank"><span>Facebookで<br>シェアする</span></a></li>';
			share_html += '<li class="twitter"><a href="http://twitter.com/share?text=' + encodeURI(document.title) + '&amp;url=' + document.URL + '" target="_blank"><span>Twitterで<br>つぶやく</span></a></li>';
			share_html += '<li class="line"><a href="line://msg/text/' + encodeURI(document.title) + '%0A%0A' + document.URL + '"><span>LINEで送る</span></a></li>';
			share_html += '</ul>';
			share_html += '</section>';
			$('#Share', elm.footer).html(share_html);
		}

		if( opt.topics_visible === true ){
			elm.footer.append('<div id="Topics" />');
			$.ajax({
				url: opt.topics_csv_path,
				type: 'GET',
				success: function(data) {
					var csv = $.csv()(data);

					topics_html += '<ul><!--';
					for( var i = 0; i < csv.length; i++){
						var indx = i;
						topics_html += '--><li><a href="' + csv[indx][1] + '"';
						if( csv[indx][3] > 0 ){
							topics_html += ' target="_blank"';
						}
						topics_html += '><img src="' + csv[indx][2] + '" alt="' + csv[indx][0] + '" width="145" height="81"></a></li><!--';
					}
					topics_html += '--></ul>';

				},
				error: function() {
					return;
				}
			}).done(function(){
				$('#Topics', elm.footer).html(topics_html);
			});
		}

		if( opt.alternate_visible === true ){
			elm.footer.append('<div id="AlternateNavi" />');
			if( opt.alternate_text.length > 0 ){
				alternate_html += '<p class="description">' + opt.alternate_text + '</p>';
			}
			if( mmc.head.alternate.length > 0 ){
				opt.alternate_url = mmc.head.alternate;
			}
			alternate_html += '<p class="button"><a href="' + opt.alternate_url.replace('http://www.mitsubishi-motors.co.jp', '') + '">三菱自動車 PCサイト</a></p>';
			$('#AlternateNavi', elm.footer).html(alternate_html);
		}

		others_html += '<div id="PageTop">';
		others_html += '<p><a href="#Page">ページの先頭へ</a></p>';
		others_html += '</div><!-- / #PageTop -->';

		others_html += '<div id="UtilityNavi">';
		others_html += '<ul>';
		others_html += '<li><a href="/sp/privacy/">個人情報保護方針</a></li>';
		others_html += '<li><a href="/sp/privacy2/">特定個人情報の適正な取扱いに関する基本方針</a></li>';
		others_html += '<li><a href="/sp/reference/">お問い合わせ</a></li>';
		others_html += '<li><a href="/sp/usage/">サイトのご利用について</a></li>';
		others_html += '</ul>';

		others_html += '<p id="Copyright">&copy; MITSUBISHI MOTORS CORPORATION. All rights reserved.</p>';
		others_html += '</div><!-- / #UtilityNavi -->';
		elm.footer.append(others_html);

		$('a', mmc.body.page).on('click.normal_link', function(){
			if ( mmc.lightbox.opened === true ) {
				return false;
			};
		});
	});
};
