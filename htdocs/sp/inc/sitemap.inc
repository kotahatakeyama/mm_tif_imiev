<div class="inner">
	<ul class="main">
		<li class="top"><a href="/sp/">トップページ</a></li>
		<li class="lineup"><a href="/sp/lineup/">カーラインアップ</a></li>
	</ul>
	<ul class="suport">
		<li class="dealership"><a href="http://map.mitsubishi-motors.co.jp/" target="_blank">販売店を検索する</a></li>
		<li class="estimate"><a href="http://try.mitsubishi-motors.co.jp/olm/EGS0001.do" target="_blank">Webで見積もる</a></li>
		<li class="catalog"><a href="https://customer.mitsubishi-motors.co.jp/olc/OlcTop.do?sourceId=OLC" target="_blank">カタログを請求する</a></li>
		<li class="test_car"><a href="http://democar.mitsubishi-motors.co.jp/democar/carSelect.html" target="_blank">試乗車／展示車を探す</a></li>
	</ul>
	<ul class="others">
		<li class="mailmagazine"><a href="/sp/mailmagazine/">メールマガジン登録</a></li>
		<li class="recall"><a href="/support/recall/">リコール情報</a></li>
		<li class="socialmedia"><a href="/sp/socialmedia/">公式ソーシャル<br>メディア一覧</a></li>
		<li class="cm"><a href="/sp/cm/">CMライブラリー</a></li>
		<li class="app"><a href="/sp/app/">公式スマートフォン<br>アプリ一覧</a></li>
		<li class="used"><a href="http://ucar.mitsubishi-motors.co.jp/" target="_blank">中古車情報<br>（M・Cネット）</a></li>
		<li class="maintenance"><a href="/support/maintenance/">メンテナンスサービス</a></li>
		<li class="eco_car"><a href="/special/imakore/">ミツビシのエコカー</a></li>
		<li class="com"><a href="http://www.mitsubishi-motors.com/jp/" target="_blank">企業サイト</a></li>
		<li class="ir"><a href="http://www.mitsubishi-motors.com/jp/sp/investors/" target="_blank">株主・投資家の<br>皆様へ</a></li>
		<li class="privacy"><a href="/sp/privacy/">個人情報保護方針</a></li>
		<li class="privacy2"><a href="/sp/privacy2/">特定個人情報の適切な<br>取扱いに関する基本方針</a></li>
		<li class="reference"><a href="/sp/reference/">お問い合わせ</a></li>
		<li class="usage"><a href="/sp/usage/">サイトのご利用について</a></li>
	</ul>
	<p class="close">閉じる</p>
<!-- / .inner --></div>
