<div class="inner">
	<div class="clip_add">
		<p class="btn"><span>このページをマイクリップに追加する</span></p>
		<p class="already_clip">このページは既にクリップ済みです。</p>
	</div><!-- /.clip_add -->

	<section class="clip_list">
		<h1>マイクリップ一覧</h1>
		
		<div class="clip_list_inner">
			
			
			<p class="no_clip">クリップしたページはありません。</p>
		</div><!-- /.clip_list_inner -->
	</section><!-- /.clip_list -->

	<section class="about_myclip">
		<h1>マイクリップ機能について</h1>
		<p>現在見ているページを保存する機能です。次回、本ウェブサイトに訪問した際にも、すぐに保存したページに移動することができます。</p>
	</section><!-- /.about_myclip -->

	<p class="close">閉じる</p>
<!-- / .inner --></div>
