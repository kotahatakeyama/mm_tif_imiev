function heartyrunNAV_write(){
var heartyrun_navi;

heartyrun_navi = '<ul><!--\n';
heartyrun_navi += '--><li id="lnav01"><a href="/heartyrun/index.html"><span>福祉車 トップ</span></a></li><!--\n';
heartyrun_navi += '--><li id="lnav02"><a href="/heartyrun/lineup.html"><span>ラインアップ</span></a></li><!--\n';
heartyrun_navi += '--><li id="lnav03"><a href="/heartyrun/price.html"><span>価格の目安</span></a></li><!--\n';
heartyrun_navi += '--><li id="lnav04"><a href="/heartyrun/grant.html"><span>税制度・助成措置</span></a></li><!--\n';
heartyrun_navi += '--><li id="lnav05"><a href="/heartyrun/event.html"><span>イベント・展示場・試乗会</span></a></li><!--\n';
heartyrun_navi += '--><li id="lnav06"><a href="/heartyrun/showroom.html"><span>常設展示場</span></a></li><!--\n';
heartyrun_navi += '--><li id="lnav07"><a href="/heartyrun/catalog.html"><span>カタログのダウンロード</span></a></li><!--\n';
heartyrun_navi += '--><li id="lnav08"><a href="/heartyrun/dealer.html"><span>取扱店ご紹介</span></a></li><!--\n';
heartyrun_navi += '--></ul>\n';
heartyrun_navi += '<p><img src="http://www.mitsubishi-motors.co.jp/share/images/localnv_im_03.gif" alt="" width="169" height="1"></p>\n';

document.write(heartyrun_navi);
}




function relatedNAV_write(carType){
var related_navi;

related_navi = '<div class="relatedLink">\n'
related_navi += '<ul><!--\n';
related_navi += '--><li><a href="http://map.mitsubishi-motors.co.jp/"><span>販売店検索</span></a></li><!--\n';
related_navi += '--><li><a href="http://democar.mitsubishi-motors.co.jp/democar/carSelect.html"><span>展示車/試乗車検索</span></a></li><!--\n';
related_navi += '--><li><a href="https://customer.mitsubishi-motors.co.jp/olc/OlcTop.do?sourceId=OLC" target="_blank"><span>カタログ請求</span></a></li><!--\n';
related_navi += '--><li><a href="/purchase/cataloglist/index.html"><span>カタログ一覧（PDF形式）</span></a></li><!--\n';
related_navi += '--></ul>\n';
related_navi += '<\/div><!-- /relatedLink -->\n';

//related_navi += '<p class="firstbn"><a href="http://www.mitsubishi-motors.tv/index.html?mv=1heartyrun_image" target="_blank"><img src="/share/images/rel_bn_04.gif" alt="HeartyRun紹介映像" width="150" height="56"></a></p>\n';
if(carType == 'delica_d5' || carType == 'm_t' || carType=='colt' || carType == 'ek' || carType == 'toppo'){
related_navi += '<div class="relatedBanner">\n';
related_navi += '<p><img src="/share/images/rel_tx_01.gif" alt="スペシャルコンテンツ" width="150" height="14"></p>\n';
if(carType == 'delica_d5' || carType == 'colt'){
related_navi += '<p><a href="http://www.aichaku.jp/index.html" target="_blank"><img src="/heartyrun/img/aichaku_bn.gif" alt="「ずっと」の時代へ−−ミツビシは、愛着力" width="150" height="56"></a></p>\n';
}
if(carType == 'delica_d5' || carType == 'm_t'){
related_navi += '<p><a href="http://www.mitsubishi-motors.tv/index.html?mv=1heartyrun_info" target="_blank"><img src="/share/images/rel_bn_05.gif" alt="福祉車（ハーティーラン）商品紹介映像" width="150" height="56"></a></p>\n';
}

if(carType == 'ek' || carType == 'toppo'){
related_navi += '<p><a href="/support/reform_heartyrun/index.html" target="_blank"><img src="/heartyrun/images/reform.gif" alt="リフォームハーティーラン" width="150" height="56"></a></p>\n';
}

related_navi += '<\/div><!-- /relatedBanner -->\n';
}

document.write(related_navi);
}
