//----------------------------------------------------------------------------
	
function spnewsData(date,carnm,maintxt,url,blankck){
	
	this.date = date;
	this.carnm = carnm;
	this.maintxt = maintxt;
	this.url = url;
	this.blankck = blankck;
	
}

//----------------------------------------------------------------------------

//
//改行は内容テキスト中に<br>をいれると改行されます
//


//（）内の説明-----------------------
//左から
//更新日（2桁表記必須 例：07.01.01）,
//タイトル,
//内容,
//リンク先（無い場合は空白）,
//ブランク判定（0は同ウィンドウ、1ならブランクで開く）''は付けない




//ここから編集可領域///////////////////////////////////////////////////////////////////////////////////////////////////////
spnews = new Array(
new spnewsData('07.02.28','Ｍｉｔｓｕｂｉｓｈｉ-Ｍｏｔｏｒｓ.ＴＶ','クルマの学校 ｉｎ Nａｅｂａ 2007 をアップしました。','http://www.mitsubishi-motors.tv/index.html',1,0,0),
new spnewsData('07.02.27','いっしょもっとｗｅｂ','壁紙（3月分カレンダー）を更新しました。','http://mmc-reds.com/wallpaper/',1,0,0),
new spnewsData('07.02.23','クルマの学校','「インテリア系」チューンアップを更新しました。','http://www.kurumano-gakko.com/tune_up/interior/interior.html',1,0,0),
new spnewsData('07.02.19','Ｍｉｔｓｕｂｉｓｈｉ-Ｍｏｔｏｒｓ.ＴＶ','パリダカトークライブ2007をアップしました。','http://www.mitsubishi-motors.tv/index.html',1),
new spnewsData('07.02.09','CMライブラリー','“ＰＡＪＥＲＯ”のテレビＣＭを更新しました。','http://www.mitsubishi-motors.co.jp/special/cm/index.html',0),
new spnewsData('07.02.09','CMライブラリー','“ＣＯＬＴ ＲＡＬＬＩＡＲＴ Ｖｅｒｓｉｏｎ-Ｒ”のテレビＣＭを追加！','http://www.mitsubishi-motors.co.jp/special/cm/index.html',0),
new spnewsData('07.02.09','ミツビシミテカラ','大試乗祭キャンペーン実施中！','http://mitsubishi-mitekara.com/',1),
new spnewsData('07.02.07','ペーパークラフト','新型デリカ Ｄ：５を追加しました。','http://www.mitsubishi-motors.co.jp/special/papercraft/index.html',0),
new spnewsData('07.02.01','ミツビシミテカラ','デリカ Ｄ：５コンテンツを追加しました。','http://mitsubishi-mitekara.com/',1),
new spnewsData('07.02.01','CMライブラリー','新型デリカ Ｄ：５のテレビＣＭを追加！','http://www.mitsubishi-motors.co.jp/special/cm/index.html',0),
new spnewsData('07.02.01','EVO ｃｌｕｂ','【壁紙ライブラリー】EVO 2月のカレンダー壁紙更新。','http://www.evoclub.net/index.html',1),
new spnewsData('06.12.07','いっしょもっとｗｅｂ','スタジアムへGO！レッズ＆ドライブ観戦レポート更新！','http://www.mmc-reds.com/gotostadium/diary/ajinomoto/',1),
new spnewsData('06.12.06','いっしょもっとｗｅｂ','浦和レッズ優勝直後のレッズバーに監督、選手が登場！','http://www.mmc-reds.com/newstopics/',1),
new spnewsData('06.12.05','モータースポーツ','【動画】増岡浩が語る新型パジェロインプレッション','http://www.mitsubishi-motors.com/motorsports/j/07dakar/index.html',1),
new spnewsData('06.12.04','モータースポーツ','WRC第16戦(最終戦)レポートが到着しました！','http://www.mitsubishi-motors.com/motorsports/j/06wrc/r16/index.html',1),
new spnewsData('06.12.02','いっしょもっとｗｅｂ','浦和レッズ優勝記念プレゼントキャンペーンスタート！','http://www.mmc-reds.com/newstopics/',1),
new spnewsData('06.12.01','GRAN-OFF','【会員限定】12月プレゼント応募受付を開始しました！','https://gran-off.jp/member/present/index.jsp',1),
new spnewsData('06.12.01','EVO ｃｌｕｂ','【壁紙ライブラリー】EVO 12月のカレンダー壁紙更新。','http://www.evoclub.net/index.html',1),
new spnewsData('06.11.28','モータースポーツ','アジア･パシフィックラリー選手権第7戦レポート到着！','http://www.mitsubishi-motors.com/motorsports/j/06aprc/r07/index.html',1),
new spnewsData('06.11.25','ミツビシミテカラ','ハッピークリスマスフェア12月24日(日)まで実施中！','http://mitsubishi-mitekara.com/html/xmas/index.html',1),
new spnewsData('06.11.24','ハロー！ハッピードライブ','ハッピークリスマスフェアの告知をアップしました。','http://happydrive.net/',1),
new spnewsData('06.11.21','キッザニア東京','スペシャルサイトがいよいよオープン！','http://www.mitsubishi-motors.co.jp/special/kidzania/index.html',1),
new spnewsData('06.11.19','クルマの学校','石川県松任店での「クルマの学校」開催レポートUP！','http://www.kurumano-gakko.com/sanka/mattou/event_report.html',1),
new spnewsData('06.11.15','EVO CLUB','EVOclubモバイルサイトオープン！','http://www.evoclub.net/member/download/mobile/index.html',1),
new spnewsData('07.01.10','GRAN-OFF','【会員限定】1月プレゼント応募受付を開始しました！','http://gran-off.jp/',1),
new spnewsData('07.01.11','モータースポーツ','2007ダカールラリー参戦レポートや動画などを毎日掲載中！','http://www.mitsubishi-motors.com/motorsports/j/07dakar/index.html',1),
new spnewsData('07.01.18','CMライブラリー','三菱自動車のテレビCMを紹介しているコンテンツが公開！','http://www.mitsubishi-motors.co.jp/special/cm/index.html',0),
new spnewsData('07.01.25','クルマの学校','なっとく！？ドライビング性格診断がアップしました。','http://www.kurumano-gakko.com/seikaku/index.html',1),
new spnewsData('07.01.25','いっしょもっとｗｅｂ','壁紙（2月分カレンダー）を更新しました。','http://mmc-reds.com/wallpaper/',1)
);


//編集可領域ここまで///////////////////////////////////////////////////////////////////////////////////////////////////////


//-----



//以下変更禁止---------------------------------------------------------------------------
function boxsort(){
	
for (i=0; i<spnews.length-1; i++) {
	for (j=i+1; j<spnews.length; j++) {
	if (spnews[j].date >= spnews[i].date) {
		 n = spnews[j];
		 spnews[j] = spnews[i];
		 spnews[i] = n;
	}
	}
	}	
}

function news_write(){
 
var writetags;

document.write('<ul>\n');

boxsort();

if(spnews.length >= 0){
	for(i=0; i<spnews.length;i++){
		writetags = '<li>';
		if(spnews[i].url){
			if(spnews[i].blankck == 1){
			blankbox = 'target="_blank">';
			}else if(spnews[i].blankck == 0){
			blankbox = '>';
			}else{
			break;	
			}
			writetags += '<a href="'+spnews[i].url+'" '+blankbox+'『'+spnews[i].carnm+'』'+spnews[i].maintxt+'（'+spnews[i].date+'）<\/a>';
		}else{
			writetags += '『'+spnews[i].carnm+'』'+spnews[i].maintxt+'（'+spnews[i].date+'）';		
		}
		writetags += '<\/li>\n';
		document.write(writetags);
		}
}
document.write('<\/ul>');
}

