//var nv_root_path="http://www.mitsubishi-motors.co.jp";


var globalnvList = new Array();

/* ホーム */
globalnvList[0] = [];

/* ラインアップ */
globalnvList[1] = [["<a href='"+nv_root_path+"/lineup/index.html'>ラインアップ トップ</a>"],
				["<a href='"+nv_root_path+"/lineup/index.html#car'>乗用車</a>"],
				["<a href='"+nv_root_path+"/lineup/index.html#ldcar'>軽自動車</a>"],
				["<a href='"+nv_root_path+"/lineup/index.html#bizcar'>商用車</a>"],
				["<a href='"+nv_root_path+"/lineup/index.html#hearty'>福祉車</a>"]];
//				["<a href='"+nv_root_path+"/lineup/index.html#accessory'>アクセサリー</a>"]];
//				["<a href='"+nv_root_path+"/lineup/index.html#custom'>乗用特装車</a>"],
//				["<a href='"+nv_root_path+"/lineup/index.html#custom_biz'>商用特装車</a>"]];

/* 購入検討サポート */
globalnvList[2] = [["<a href='"+nv_root_path+"/purchase/index.html'>購入検討サポート トップ</a>"],
				["<a href='"+nv_root_path+"/special/imakore/index.html' target='_blank'>ミツビシのエコカー</a>"],
				["<a href='"+nv_root_path+"/share/log/dealer_search.html'>販売店検索</a>"],
				["<a href='"+nv_root_path+"/share/log/demo_car.html'>展示車／試乗車検索</a>"],
				["<a href='"+nv_root_path+"/share/log/onlinequote.html'>オンライン見積もり</a>"],
				["<a href='https://customer.mitsubishi-motors.co.jp/olc/OlcTop.do?sourceId=OLC' target='_blank'>カタログ請求</a>"],
				["<a href='"+nv_root_path+"/purchase/cataloglist/index.html'>カタログ一覧</a>"],
				["<a href='"+nv_root_path+"/support/accessory/index.html'>アクセサリー</a>"],
				["<a href='"+nv_root_path+"/support/maintenance/service/assist24.html'>三菱アシスト24</a>"],
				["<a href='"+nv_root_path+"/purchase/diacard/index.html'>Diaカード</a>"],
				["<a href='"+nv_root_path+"/purchase/karunori/index.html'>かる乗り</a>"],
				["<a href='http://www.mmc-dia-finance.com/' target='_blank'>クレジット・リース</a>"],
				["<a href='http://ucar.mitsubishi-motors.co.jp/ucar/'>中古車情報</a>"],
				["<a href='"+nv_root_path+"/reference/index.html'>お問い合わせ</a>"]];

/* ユーザーサポート/カーライフ */
globalnvList[3] = [["<a href='"+nv_root_path+"/support/index.html'>ユーザーサポート/カーライフ トップ</a>"],
				["<a href='"+nv_root_path+"/support/maintenance/index.html'>メンテナンス</a>"],
				["<a href='"+nv_root_path+"/support/download/index.html'>ダウンロード</a>"],
				["<a href='"+nv_root_path+"/support/recall/index.html'>リコール等の重要なお知らせ</a>"],
				["<a href='"+nv_root_path+"/support/manual/index.html'>取扱説明書一覧</a>"],
				["<a href='"+nv_root_path+"/support/safety/index.html'>安全なドライブのために</a>"],
				["<a href='"+nv_root_path+"/support/maintenance/service/assist24.html'>三菱アシスト24</a>"],
				["<a href='"+nv_root_path+"/purchase/diacard/index.html'>Diaカード</a>"],
				["<a href='http://www.mmc-dia-finance.com/' target='_blank'>MMCレンタカー</a>"]];

/* モータースポーツ */
globalnvList[4] = [["<a href='http://www.mitsubishi-motors.co.jp/motorsports/index.html'>日本語</a>"],
				["<a href='http://www.mitsubishi-motors.com/special/motorsports/' target='_blank'>English</a>"]];

/* スペシャルコンテンツ */
globalnvList[5] = [["<a href='"+nv_root_path+"/special/index.html'>スペシャルコンテンツ トップ</a>"],
				["<a href='"+nv_root_path+"/special/index.html#special03'>企業からの情報</a>"],
				["<a href='"+nv_root_path+"/special/index.html#special04'>EVポータル</a>"],
				["<a href='"+nv_root_path+"/special/index.html#special05'>モーターショー</a>"],
				["<a href='"+nv_root_path+"/special/index.html#special02'>キャンペーンサイト</a>"],
				["<a href='"+nv_root_path+"/special/index.html#special01'>エンターテインメント</a>"],
				["<a href='"+nv_root_path+"/special/archive/index.html'>スペシャルコンテンツアーカイブ</a>"]];

/* 企業情報 */
globalnvList[6] = [["<a href='"+nv_root_path+"/corporate/index.html'>企業情報 トップ</a>"],
				["<a href='"+nv_root_path+"/corporate/pressrelease/index.html'>プレスリリース</a>"],
				["<a href='"+nv_root_path+"/corporate/philosophy/index.html'>経営の考え方</a>"],
				["<a href='"+nv_root_path+"/corporate/aboutus/index.html'>三菱自動車の概要</a>"],
				["<a href='"+nv_root_path+"/corporate/facilities/index.html'>事業所・関連施設</a>"],
				["<a href='"+nv_root_path+"/corporate/technology/index.html'>クルマの技術</a>"],
				["<a href='"+nv_root_path+"/corporate/ir/index.html'>投資家情報（IR）</a>"],
				["<a href='"+nv_root_path+"/corporate/recruit/index.html'>採用情報</a>"]];

/* 社会環境への取り組み */
globalnvList[7] = [["<a href='"+nv_root_path+"/social/index.html'>社会・環境への取り組み トップ</a>"],
				["<a href='"+nv_root_path+"/social/message.html'>トップメッセージ</a>"],
				["<a href='"+nv_root_path+"/social/csr/index.html'>CSRへの取り組み</a>"],
				["<a href='"+nv_root_path+"/social/approach/index.html'>社会への取り組み</a>"],
				["<a href='"+nv_root_path+"/social/environment/index.html'>環境への取り組み</a>"],
				["<a href='"+nv_root_path+"/social/ethics_com/index.html'>企業倫理委員会</a>"],
				["<a href='"+nv_root_path+"/social/contribution/index.html'>社会貢献活動</a>"],
				["<a href='"+nv_root_path+"/social/report/index.html'>三菱自動車 社会・環境報告書</a>"]];


