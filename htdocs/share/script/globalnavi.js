/* base */
floating = 0;
minx = 0;
maxx = 0;
dtype = 0;
bdata = 0;



/* url set */
var impath="http://www.mitsubishi-motors.co.jp/share/images/";


/* browser */
if (windows){
	if (opera){
		bdata=0;
	} else if (ie){
		bdata=1;
		document.write('<link rel="stylesheet" type="text/css" href="http://www.mitsubishi-motors.co.jp/share/style/win_ie.css" media="all">');
	} else if (gecko){
		bdata=2;
	}
} else if (mac){
	if (opera){
		bdata=0;
	} else if (safari){
		bdata=2;
		document.write('<link rel="stylesheet" type="text/css" href="http://www.mitsubishi-motors.co.jp/share/style/osx_safari.css" media="all">');
	} else if (gecko){
		bdata=2;
		document.write('<link rel="stylesheet" type="text/css" href="http://www.mitsubishi-motors.co.jp/share/style/osx_gecko.css" media="all">');
	}
}


/* preload image */
var glbImg = new Array();
var n = 8;
var iwk;
for (i=0; i<n; i++) {
	iwk=i+1;
	glbImg[i] = impath+"global_nv_0" + iwk + "_a.gif";
}

var floatImg = [
"glcar_im_01.jpg","glcar_im_02.jpg","glcar_im_03.jpg","glcar_im_04.jpg","glcar_im_05.jpg","glcar_im_06.jpg","glcar_im_07.jpg","glcar_im_08.jpg","glcar_im_09.jpg","glcar_im_10.jpg","glcar_im_11.jpg","glcar_im_12.jpg","glcar_im_13.jpg","glcar_im_14.jpg","glcar_im_15.jpg","glcar_im_16.jpg",
"glldcar_im_01.jpg","glldcar_im_02.jpg","glldcar_im_03.jpg","glldcar_im_04.jpg",
"glbizcar_im_01.jpg","glbizcar_im_02.jpg","glbizcar_im_03.jpg","glbizcar_im_04.jpg","glbizcar_im_05.jpg","glbizcar_im_06.jpg",
"glspecial_im_01.jpg","glspecial_im_02.jpg","glspecial_im_03.jpg","glspecial_im_04.jpg","glspecial_im_05.jpg","glspecial_im_06.jpg","glspecial_im_07.jpg","glspecial_im_08.jpg","glspecial_im_09.jpg","glspecial_im_10.jpg",
"glspecial_im_11.jpg","glspecial_im_12.jpg","glspecial_im_13.jpg","glspecial_im_14.jpg","glspecial_im_15.jpg","glspecial_im_16.jpg","glspecial_im_17.jpg","glspecial_im_18.jpg"
];

for (i=0; i<=floatImg.length; i++) {
	preloadimgArray(impath+floatImg[i]);
}


/* swap image */
function swapglb(num){
	img=impath+"global_nv_"+num+".gif";
	nm="global_nv_"+num.substring(0,2);
	swapimage(nm,img);
}



/* navigation data */
var gn11Cnt = new Array();
gn11Cnt[0] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/lineup/index.html#car\" id=\"gn11a\" onMouseOver=\"callglb(\'car2\')\;callglbs(gn12aData,\'gn12\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"right\">乗用車</span></a></li>";
gn11Cnt[1] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/lineup/index.html#ldcar\" id=\"gn11b\" onMouseOver=\"callglb(\'car2\')\;callglbs(gn12bData,\'gn12\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"right\">軽自動車</span></a></li>";
gn11Cnt[2] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/lineup/index.html#bizcar\" id=\"gn11c\" onMouseOver=\"callglb(\'car2\')\;callglbs(gn12cData,\'gn12\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"right\">商用車</span></a></li>";
gn11Cnt[3] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/heartyrun/index.html\" id=\"gn11d\" onMouseOver=\"resetinv(\'gn13\')\;resetinv(\'gn12\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>福祉車(ハーティーラン)</span></a></li>";
gn11Cnt[4] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/customize/index.html\" id=\"gn11e\" onMouseOver=\"resetinv(\'gn13\')\;resetinv(\'gn12\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>カスタムカー</span></a></li>";
var gn11Data = "<ul>"+gn11Cnt[0]+gn11Cnt[1]+gn11Cnt[2]+gn11Cnt[3]+gn11Cnt[4]+"</ul>";

var gn12aCnt = new Array();
gn12aCnt[0] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/outlander/index.html\" id=\"gn12aa\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13aaData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11a\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11a\')\;return false\;\"><span class=\"right\">アウトランダー</span></a></li>";
gn12aCnt[2] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/eclipse_spd/index.html\" id=\"gn12ac\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13acData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11a\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11a\')\;return false\;\"><span class=\"right\">エクリプス スパイダー</span></a></li>";
gn12aCnt[4] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/grandis/index.html\" id=\"gn12ae\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13aeData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11a\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11a\')\;return false\;\"><span class=\"right\">グランディス</span></a></li>";
gn12aCnt[5] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/colt/index.html\" id=\"gn12af\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13afData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11a\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11a\')\;return false\;\"><span class=\"right\">コルト</span></a></li>";
gn12aCnt[6] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/colt_plus/index.html\" id=\"gn12ag\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13agData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11a\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11a\')\;return false\;\"><span class=\"right\">コルト プラス</span></a></li>";
gn12aCnt[8] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/dion/index.html\" id=\"gn12ai\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13aiData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11a\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11a\')\;return false\;\"><span class=\"right\">ディオン</span></a></li>";
gn12aCnt[9] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/spacegear/index.html\" id=\"gn12aj\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13ajData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11a\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11a\')\;return false\;\"><span class=\"right\">デリカ スペースギア</span></a></li>";
gn12aCnt[10] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/pajero/index.html\" id=\"gn12ak\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13akData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11a\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11a\')\;return false\;\"><span class=\"right\">パジェロ</span></a></li>";
gn12aCnt[11] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/pajero_io/index.html\" id=\"gn12al\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13alData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11a\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11a\')\;return false\;\"><span class=\"right\">パジェロイオ</span></a></li>";
gn12aCnt[12] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/lancer/index.html\" id=\"gn12am\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13amData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11a\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11a\')\;return false\;\"><span class=\"right\">ランサー</span></a></li>";
gn12aCnt[13] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/lancer_wagon/index.html\" id=\"gn12an\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13anData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11a\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11a\')\;return false\;\"><span class=\"right\">ランサー ワゴン</span></a></li>";
gn12aCnt[14] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/evo/index.html\" id=\"gn12ao\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13aoData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11a\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11a\')\;return false\;\"><span class=\"right\">ランサー エボリューション</span></a></li>";
gn12aCnt[15] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/evo_wagon/index.html\" id=\"gn12ap\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13apData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11a\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11a\')\;return false\;\"><span class=\"right\">ランサー エボリューション ワゴン</span></a></li>";
var gn12aData = "<ul>"+gn12aCnt[0]+gn12aCnt[2]+gn12aCnt[4]+gn12aCnt[5]+gn12aCnt[6]+gn12aCnt[8]+gn12aCnt[9]+gn12aCnt[10]+gn12aCnt[11]+gn12aCnt[12]+gn12aCnt[13]+gn12aCnt[14]+gn12aCnt[15]+"</ul>";

var gn12bCnt = new Array();
gn12bCnt[0] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/ek/index.html\" id=\"gn12ba\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13baData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11b\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11b\')\;return false\;\"><span class=\"right\">eK</span></a></li>";
gn12bCnt[1] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/tbox/index.html\" id=\"gn12bb\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13bbData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11b\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11b\')\;return false\;\"><span class=\"right\">タウンボックス</span></a></li>";
gn12bCnt[2] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/pajero_mini/index.html\" id=\"gn12bc\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13bcData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11b\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11b\')\;return false\;\"><span class=\"right\">パジェロミニ</span></a></li>";
gn12bCnt[3] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/minica/index.html\" id=\"gn12bd\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13bdData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11b\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11b\')\;return false\;\"><span class=\"right\">ミニカ</span></a></li>";
var gn12bData = "<ul>"+gn12bCnt[0]+gn12bCnt[1]+gn12bCnt[2]+gn12bCnt[3]+"</ul>";

var gn12cCnt = new Array();
gn12cCnt[0] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/delica_cargo/index.html\" id=\"gn12ca\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13caData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11c\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11c\')\;return false\;\"><span class=\"right\">デリカカーゴ</span></a></li>";
gn12cCnt[1] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/delica_truck/index.html\" id=\"gn12cb\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13cbData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11c\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11c\')\;return false\;\"><span class=\"right\">デリカトラック</span></a></li>";
gn12cCnt[2] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/delica_van/index.html\" id=\"gn12cc\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13ccData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11c\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11c\')\;return false\;\"><span class=\"right\">デリカバン</span></a></li>";
gn12cCnt[3] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/minicab_truck/index.html\" id=\"gn12cd\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13cdData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11c\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11c\')\;return false\;\"><span class=\"right\">ミニキャブトラック</span></a></li>";
gn12cCnt[4] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/minicab_van/index.html\" id=\"gn12ce\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13ceData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11c\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11c\')\;return false\;\"><span class=\"right\">ミニキャブバン</span></a></li>";
gn12cCnt[5] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/lancer_cargo/index.html\" id=\"gn12cf\" onMouseOver=\"callglb(\'car3\')\;callglbs(gn13cfData,\'gn13\')\;gnon(this.id)\;gnon(\'gn11c\')\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11c\')\;return false\;\"><span class=\"right\">ランサー カーゴ</span></a></li>";
gn12cCnt[6] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/customized_bizcar/index.html\" id=\"gn12cg\" onMouseOver=\"resetinv(\'gn13\')\;gnof(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11c\')\;return false\;\"><span>商用特装車</span></a></li>";
gn12cCnt[7] = "<li><a href=\"http://www.mitsubishi-fuso.com/\" target=\"_blank\" id=\"gn12ch\" onMouseOver=\"resetinv(\'gn13\')\;gnof(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;gnof(\'gn11c\')\;return false\;\"><span>トラック・バス</span></a></li>";
var gn12cData = "<ul>"+gn12cCnt[0]+gn12cCnt[1]+gn12cCnt[2]+gn12cCnt[3]+gn12cCnt[4]+gn12cCnt[5]+gn12cCnt[6]+gn12cCnt[7]+"</ul>";

var gn13aaData = "<a href=\"http://www.mitsubishi-motors.co.jp/outlander/index.html\" onMouseOver=\"gnon(\'gn11a\')\;gnon(\'gn12aa\')\;return false\;\" onMouseOut=\"gnof(\'gn11a\')\;gnof(\'gn12aa\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glcar_im_01.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13abData = "<a href=\"http://www.mitsubishi-motors.co.jp/airtrek/index.html\" onMouseOver=\"gnon(\'gn11a\')\;gnon(\'gn12ab\')\;return false\;\" onMouseOut=\"gnof(\'gn11a\')\;gnof(\'gn12ab\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glcar_im_02.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13acData = "<a href=\"http://www.mitsubishi-motors.co.jp/eclipse_spd/index.html\" onMouseOver=\"gnon(\'gn11a\')\;gnon(\'gn12ac\')\;return false\;\" onMouseOut=\"gnof(\'gn11a\')\;gnof(\'gn12ac\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glcar_im_03.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13aeData = "<a href=\"http://www.mitsubishi-motors.co.jp/grandis/index.html\" onMouseOver=\"gnon(\'gn11a\')\;gnon(\'gn12ae\')\;return false\;\" onMouseOut=\"gnof(\'gn11a\')\;gnof(\'gn12ae\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glcar_im_05.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13afData = "<a href=\"http://www.mitsubishi-motors.co.jp/colt/index.html\" onMouseOver=\"gnon(\'gn11a\')\;gnon(\'gn12af\')\;return false\;\" onMouseOut=\"gnof(\'gn11a\')\;gnof(\'gn12af\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glcar_im_06.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13agData = "<a href=\"http://www.mitsubishi-motors.co.jp/colt_plus/index.html\" onMouseOver=\"gnon(\'gn11a\')\;gnon(\'gn12ag\')\;return false\;\" onMouseOut=\"gnof(\'gn11a\')\;gnof(\'gn12ag\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glcar_im_07.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13aiData = "<a href=\"http://www.mitsubishi-motors.co.jp/dion/index.html\" onMouseOver=\"gnon(\'gn11a\')\;gnon(\'gn12ai\')\;return false\;\" onMouseOut=\"gnof(\'gn11a\')\;gnof(\'gn12ai\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glcar_im_09.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13ajData = "<a href=\"http://www.mitsubishi-motors.co.jp/spacegear/index.html\" onMouseOver=\"gnon(\'gn11a\')\;gnon(\'gn12aj\')\;return false\;\" onMouseOut=\"gnof(\'gn11a\')\;gnof(\'gn12aj\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glcar_im_10.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13akData = "<a href=\"http://www.mitsubishi-motors.co.jp/pajero/index.html\" onMouseOver=\"gnon(\'gn11a\')\;gnon(\'gn12ak\')\;return false\;\" onMouseOut=\"gnof(\'gn11a\')\;gnof(\'gn12ak\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glcar_im_11.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13alData = "<a href=\"http://www.mitsubishi-motors.co.jp/pajero_io/index.html\" onMouseOver=\"gnon(\'gn11a\')\;gnon(\'gn12al\')\;return false\;\" onMouseOut=\"gnof(\'gn11a\')\;gnof(\'gn12al\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glcar_im_12.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13amData = "<a href=\"http://www.mitsubishi-motors.co.jp/lancer/index.html\" onMouseOver=\"gnon(\'gn11a\')\;gnon(\'gn12am\')\;return false\;\" onMouseOut=\"gnof(\'gn11a\')\;gnof(\'gn12am\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glcar_im_13.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13anData = "<a href=\"http://www.mitsubishi-motors.co.jp/lancer_wagon/index.html\" onMouseOver=\"gnon(\'gn11a\')\;gnon(\'gn12an\')\;return false\;\" onMouseOut=\"gnof(\'gn11a\')\;gnof(\'gn12an\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glcar_im_14.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13aoData = "<a href=\"http://www.mitsubishi-motors.co.jp/evo/index.html\" onMouseOver=\"gnon(\'gn11a\')\;gnon(\'gn12ao\')\;return false\;\" onMouseOut=\"gnof(\'gn11a\')\;gnof(\'gn12ao\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glcar_im_15.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13apData = "<a href=\"http://www.mitsubishi-motors.co.jp/evo_wagon/index.html\" onMouseOver=\"gnon(\'gn11a\')\;gnon(\'gn12ap\')\;return false\;\" onMouseOut=\"gnof(\'gn11a\')\;gnof(\'gn12ap\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glcar_im_16.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";

var gn13baData = "<a href=\"http://www.mitsubishi-motors.co.jp/ek/index.html\" onMouseOver=\"gnon(\'gn11b\')\;gnon(\'gn12ba\')\;return false\;\" onMouseOut=\"gnof(\'gn11b\')\;gnof(\'gn12ba\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glldcar_im_01.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13bbData = "<a href=\"http://www.mitsubishi-motors.co.jp/tbox/index.html\" onMouseOver=\"gnon(\'gn11b\')\;gnon(\'gn12bb\')\;return false\;\" onMouseOut=\"gnof(\'gn11b\')\;gnof(\'gn12bb\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glldcar_im_02.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13bcData = "<a href=\"http://www.mitsubishi-motors.co.jp/pajero_mini/index.html\" onMouseOver=\"gnon(\'gn11b\')\;gnon(\'gn12bc\')\;return false\;\" onMouseOut=\"gnof(\'gn11b\')\;gnof(\'gn12bc\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glldcar_im_03.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13bdData = "<a href=\"http://www.mitsubishi-motors.co.jp/minica/index.html\" onMouseOver=\"gnon(\'gn11b\')\;gnon(\'gn12bd\')\;return false\;\" onMouseOut=\"gnof(\'gn11b\')\;gnof(\'gn12bd\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glldcar_im_04.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";

var gn13caData = "<a href=\"http://www.mitsubishi-motors.co.jp/delica_cargo/index.html\" onMouseOver=\"gnon(\'gn11c\')\;gnon(\'gn12ca\')\;return false\;\" onMouseOut=\"gnof(\'gn11c\')\;gnof(\'gn12ca\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glbizcar_im_01.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13cbData = "<a href=\"http://www.mitsubishi-motors.co.jp/delica_truck/index.html\" onMouseOver=\"gnon(\'gn11c\')\;gnon(\'gn12cb\')\;return false\;\" onMouseOut=\"gnof(\'gn11c\')\;gnof(\'gn12cb\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glbizcar_im_02.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13ccData = "<a href=\"http://www.mitsubishi-motors.co.jp/delica_van/index.html\" onMouseOver=\"gnon(\'gn11c\')\;gnon(\'gn12cc\')\;return false\;\" onMouseOut=\"gnof(\'gn11c\')\;gnof(\'gn12cc\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glbizcar_im_03.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13cdData = "<a href=\"http://www.mitsubishi-motors.co.jp/minicab_truck/index.html\" onMouseOver=\"gnon(\'gn11c\')\;gnon(\'gn12cd\')\;return false\;\" onMouseOut=\"gnof(\'gn11c\')\;gnof(\'gn12cd\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glbizcar_im_04.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13ceData = "<a href=\"http://www.mitsubishi-motors.co.jp/minicab_van/index.html\" onMouseOver=\"gnon(\'gn11c\')\;gnon(\'gn12ce\')\;return false\;\" onMouseOut=\"gnof(\'gn11c\')\;gnof(\'gn12ce\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glbizcar_im_05.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn13cfData = "<a href=\"http://www.mitsubishi-motors.co.jp/lancer_cargo/index.html\" onMouseOver=\"gnon(\'gn11c\')\;gnon(\'gn12cf\')\;return false\;\" onMouseOut=\"gnof(\'gn11c\')\;gnof(\'gn12cf\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glbizcar_im_06.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";

var gn21Cnt = new Array();
gn21Cnt[0] = "<li><a href=\"http://www.mapion.co.jp/custom/mitsubishi/index.html\" target=\"_blank\" id=\"gn21a\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>販売店検索</span></a></li>";
gn21Cnt[1] = "<li><a href=\"http://democar.mitsubishi-motors.co.jp/democar/carSelect.html\" target=\"_blank\" id=\"gn21b\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>展示車/試乗車検索</span></a></li>";
gn21Cnt[2] = "<li><a href=\"http://showroom.mitsubishi-motors.co.jp/online/source/counter1.asp\" target=\"_blank\" id=\"gn21c\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>カタログ請求</span></a></li>";
gn21Cnt[3] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/purchase/cataloglist/index.html\" id=\"gn21d\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>カタログ一覧</span></a></li>";
gn21Cnt[4] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/purchase/pricesearch/index.html\" id=\"gn21e\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>価格帯で探す</span></a></li>";
gn21Cnt[5] = "<li><a href=\"http://sirius.mitsubishi-motors.co.jp/mcnet/index.asp\" target=\"_blank\" id=\"gn21f\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>中古車情報</span></a></li>";
var gn21Data = "<ul>"+gn21Cnt[0]+gn21Cnt[1]+gn21Cnt[2]+gn21Cnt[3]+gn21Cnt[4]+gn21Cnt[5]+"</ul>";

var gn31Cnt = new Array();
gn31Cnt[0] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/support/accessory/index.html\" id=\"gn31a\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span><span>アクセサリー</span></a></li>";
gn31Cnt[1] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/support/maintenance/index.html\" id=\"gn31b\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>メンテナンス</span></a></li>";
gn31Cnt[2] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/social/environment/recyclefee/index.html\" target=\"_blank\" id=\"gn31c\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>自動車リサイクル料金</span></a></li>";
gn31Cnt[3] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/support/diacard/index.html\" id=\"gn31d\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>Diaカード</span></a></li>";
gn31Cnt[4] = "<li><a href=\"http://me.mitsubishi-motors.co.jp/mailmagazine/\" target=\"_blank\" id=\"gn31e\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>メールマガジンの登録</span></a></li>";
gn31Cnt[5] = "<li><a href=\"http://recall.mitsubishi-motors.co.jp/Recall/ListRecall.do\" target=\"_blank\" id=\"gn31f\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>リコール情報</span></a></li>";
var gn31Data = "<ul>"+gn31Cnt[0]+gn31Cnt[1]+gn31Cnt[2]+gn31Cnt[3]+gn31Cnt[4]+gn31Cnt[5]+"</ul>";

var gn51Cnt = new Array();
gn51Cnt[1] = "<li><a href=\"http://www.mitsubishi-motors.tv/index.html\" target=\"_blank\" id=\"gn51b\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52bData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">Mitsubishi-Motors.TV</span></a></li>";
gn51Cnt[2] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/special/museum/index.html\" id=\"gn51c\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52cData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">ウェブ・ミュージアム</span></a></li>";
gn51Cnt[3] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/special/safety/index.html\" id=\"gn51d\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52dData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">安全なドライブのために</span></a></li>";
gn51Cnt[4] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/social/exchange/kids/index.html\" id=\"gn51e\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52eData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">こどもクルマミュージアム</span></a></li>";
gn51Cnt[6] = "<li><a href=\"http://www.mmc-reds.com/\" target=\"_blank\" id=\"gn51g\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52gData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">いっしょもっとWEB</span></a></li>";
gn51Cnt[7] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/special/papercraft/index.html\" id=\"gn51h\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52hData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">ペーパークラフト</span></a></li>";
gn51Cnt[8] = "<li><a href=\"http://www.mitsubishi-motors-showroom.com/\" target=\"_blank\" id=\"gn51r\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52rData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">ショールーム</span></a></li>";
gn51Cnt[9] = "<li class=\"parent\"><a href=\"http://www.mitsubishi-motors.co.jp/special/index.html#special02\" id=\"gn51i\" onMouseOver=\"resetinv(\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>車種別スペシャルコンテンツ</span></a></li>";
gn51Cnt[10] = "<li class=\"children\"><a href=\"http://outlander.jp/\" target=\"_blank\" id=\"gn51j\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52jData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">アウトランダー</span></a></li>";
gn51Cnt[11] = "<li class=\"children\"><a href=\"http://mitsubishi-i.jp/\" target=\"_blank\" id=\"gn51k\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52kData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">i 【アイ】</span></a></li>";
gn51Cnt[12] = "<li class=\"children\"><a href=\"http://www.mitsubishi-motors.co.jp/ek/special/index.html\" target=\"_blank\" id=\"gn51l\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52lData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">eKスペシャルサイト</span></a></li>";
gn51Cnt[13] = "<li class=\"children\"><a href=\"http://www.mitsubishi-motors.co.jp/grandis/special/special.html\" target=\"_blank\" id=\"gn51m\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52mData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">グランディス</span></a></li>";
gn51Cnt[14] = "<li class=\"children\"><a href=\"http://www.mitsubishi-motors.co.jp/colt/special/index.html\" target=\"_blank\" id=\"gn51n\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52nData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">コルトの魅力</span></a></li>";
gn51Cnt[15] = "<li class=\"children\"><a href=\"http://www.mitsubishi-motors.co.jp/colt_plus/special/index.html\" target=\"_blank\" id=\"gn51o\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52oData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">コルトプラスの魅力</span></a></li>";
gn51Cnt[17] = "<li class=\"children\"><a href=\"http://www.evoclub.net/index.html\" target=\"_blank\" id=\"gn51q\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52qData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">EVO CLUB</span></a></li>";
gn51Cnt[18] = "<li><a href=\"http://c-stylebook.jp/\" target=\"_blank\" id=\"gn51s\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52sData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">C-style book</span></a></li>";
gn51Cnt[19] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/gran-off/\" target=\"_blank\" id=\"gn51t\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52tData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">GRAN-OFF</span></a></li>";
gn51Cnt[20] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/special/archive/index.html\" id=\"gn51u\" onMouseOver=\"callglb(\'special2\')\;callglbs(gn52uData,\'gn52\')\;gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span class=\"left\">スペシャルコンテンツ アーカイブ</span></a></li>";
var gn51Data = "<ul>"+gn51Cnt[1]+gn51Cnt[19]+gn51Cnt[18]+gn51Cnt[2]+gn51Cnt[3]+gn51Cnt[4]+gn51Cnt[6]+gn51Cnt[7]+gn51Cnt[8]+gn51Cnt[9]+gn51Cnt[10]+gn51Cnt[11]+gn51Cnt[12]+gn51Cnt[13]+gn51Cnt[14]+gn51Cnt[15]+gn51Cnt[17]+gn51Cnt[20]+"</ul>";

var gn52bData = "<a href=\"http://www.mitsubishi-motors.tv/index.html\" target=\"_blank\" onMouseOver=\"gnon(\'gn51b\')\;return false\;\" onMouseOut=\"gnof(\'gn51b\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_02.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52cData = "<a href=\"http://www.mitsubishi-motors.co.jp/special/museum/index.html\" onMouseOver=\"gnon(\'gn51c\')\;return false\;\" onMouseOut=\"gnof(\'gn51c\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_03.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52dData = "<a href=\"http://www.mitsubishi-motors.co.jp/special/safety/index.html\" onMouseOver=\"gnon(\'gn51d\')\;return false\;\" onMouseOut=\"gnof(\'gn51d\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_04.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52eData = "<a href=\"http://www.mitsubishi-motors.co.jp/social/exchange/kids/index.html\" onMouseOver=\"gnon(\'gn51e\')\;return false\;\" onMouseOut=\"gnof(\'gn51e\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_05.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52gData = "<a href=\"http://www.mmc-reds.com/\" target=\"_blank\" onMouseOver=\"gnon(\'gn51g\')\;return false\;\" onMouseOut=\"gnof(\'gn51g\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_07.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52hData = "<a href=\"http://www.mitsubishi-motors.co.jp/special/papercraft/index.html\" onMouseOver=\"gnon(\'gn51h\')\;return false\;\" onMouseOut=\"gnof(\'gn51h\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_08.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52rData = "<a href=\"http://www.mitsubishi-motors-showroom.com/\" target=\"_blank\" onMouseOver=\"gnon(\'gn51r\')\;return false\;\" onMouseOut=\"gnof(\'gn51r\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_18.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52jData = "<a href=\"http://outlander.jp/\" target=\"_blank\" onMouseOver=\"gnon(\'gn51j\')\;return false\;\" onMouseOut=\"gnof(\'gn51j\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_10.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52kData = "<a href=\"http://mitsubishi-i.jp/\" target=\"_blank\" onMouseOver=\"gnon(\'gn51k\')\;return false\;\" onMouseOut=\"gnof(\'gn51k\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_11.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52lData = "<a href=\"http://www.mitsubishi-motors.co.jp/ek/special/index.html\" target=\"_blank\" onMouseOver=\"gnon(\'gn51l\')\;return false\;\" onMouseOut=\"gnof(\'gn51l\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_19.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52mData = "<a href=\"http://www.mitsubishi-motors.co.jp/grandis/special/special.html\" target=\"_blank\" onMouseOver=\"gnon(\'gn51m\')\;return false\;\" onMouseOut=\"gnof(\'gn51m\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_13.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52nData = "<a href=\"http://www.mitsubishi-motors.co.jp/colt/special/index.html\" target=\"_blank\" onMouseOver=\"gnon(\'gn51n\')\;return false\;\" onMouseOut=\"gnof(\'gn51n\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_14.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52oData = "<a href=\"http://www.mitsubishi-motors.co.jp/colt_plus/special/index.html\" target=\"_blank\" onMouseOver=\"gnon(\'gn51o\')\;return false\;\" onMouseOut=\"gnof(\'gn51o\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_15.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52qData = "<a href=\"http://www.evoclub.net/index.html\" target=\"_blank\" onMouseOver=\"gnon(\'gn51q\')\;return false\;\" onMouseOut=\"gnof(\'gn51q\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_17.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52sData = "<a href=\"http://c-stylebook.jp/\" target=\"_blank\" onMouseOver=\"gnon(\'gn51s\')\;return false\;\" onMouseOut=\"gnof(\'gn51s\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_20.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52tData = "<a href=\"http://www.mitsubishi-motors.co.jp/gran-off/\" target=\"_blank\" onMouseOver=\"gnon(\'gn51t\')\;return false\;\" onMouseOut=\"gnof(\'gn51t\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_21.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";
var gn52uData = "<a href=\"http://www.mitsubishi-motors.co.jp/special/archive/index.html\" onMouseOver=\"gnon(\'gn51u\')\;return false\;\" onMouseOut=\"gnof(\'gn51u\')\;return false\;\"><img src=\"http://www.mitsubishi-motors.co.jp/share/images/float/glspecial_im_22.jpg\" alt=\"\" width=\"342\" height=\"418\"></a>";

var gn61Cnt = new Array();
gn61Cnt[0] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/corporate/message/index.html\" id=\"gn61a\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>トップメッセージと企業理念</span></a></li>";
gn61Cnt[1] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/corporate/pressrelease/index.html\" id=\"gn61b\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>プレスリリース</span></a></li>";
gn61Cnt[2] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/corporate/aboutus/index.html\" id=\"gn61c\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>三菱自動車の概要</span></a></li>";
gn61Cnt[3] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/corporate/technology/index.html\" target=\"_blank\" id=\"gn61d\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>最新技術</span></a></li>";
gn61Cnt[4] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/corporate/ir/index.html\" id=\"gn61e\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>投資家情報(IR)</span></a></li>";
gn61Cnt[5] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/corporate/recruit/index.html\" id=\"gn61f\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>採用情報</span></a></li>";
var gn61Data = "<ul>"+gn61Cnt[0]+gn61Cnt[1]+gn61Cnt[2]+gn61Cnt[3]+gn61Cnt[4]+gn61Cnt[5]+"</ul>";

var gn71Cnt = new Array();
gn71Cnt[0] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/social/ethics/index.html\" id=\"gn71a\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>企業倫理への取り組み</span></a></li>";
gn71Cnt[1] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/social/environment/index.html\" target=\"_blank\" id=\"gn71b\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>環境への取り組み</span></a></li>";
gn71Cnt[2] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/social/quality/index.html\" id=\"gn71c\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>品質向上への取り組み</span></a></li>";
gn71Cnt[3] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/social/trafficsafety/index.html\" id=\"gn71d\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>交通安全への取り組み</span></a></li>";
gn71Cnt[4] = "<li><a href=\"http://www.mitsubishi-motors.co.jp/social/exchange/index.html\" id=\"gn71e\" onMouseOver=\"gnon(this.id)\;return false\;\" onMouseOut=\"gnof(this.id)\;return false\;\"><span>社会との交流</span></a></li>";
var gn71Data = "<ul>"+gn71Cnt[0]+gn71Cnt[1]+gn71Cnt[2]+gn71Cnt[3]+gn71Cnt[4]+"</ul>";



/* for float */
if (document.all) {
}else{
window.captureEvents(Event.MOUSEMOVE);
}

function callglb(num){
if (bdata!=0){

if (num=="car1") {
resetall();
minx=60;
maxx=249;
document.getElementById('gn').style.display = 'block';
document.getElementById('gn11').style.display = 'block';
getIndex(gn11Data,'gn11');
}
if (num=="car2") {
minx=60;
maxx=437;
document.getElementById('gn12').style.display = 'block';
}
if (num=="car3") {
minx=60;
maxx=781;
document.getElementById('gn13').style.display = 'block';
}
if (num=="purchase1") {
resetall();
minx=148;
maxx=337;
document.getElementById('gn').style.display = 'block';
document.getElementById('gn21').style.display = 'block';
getIndex(gn21Data,'gn21');
}
if (num=="support1") {
resetall();
minx=244;
maxx=433;
document.getElementById('gn').style.display = 'block';
document.getElementById('gn31').style.display = 'block';
getIndex(gn31Data,'gn31');
}
if (num=="special1") {
resetall();
minx=485;
maxx=674;
document.getElementById('gn').style.display = 'block';
document.getElementById('gn51').style.display = 'block';
getIndex(gn51Data,'gn51');
}
if (num=="special2") {
minx=142;
maxx=674;
document.getElementById('gn52').style.display = 'block';
}
if (num=="corporate1") {
resetall();
minx=592;
maxx=781;
document.getElementById('gn').style.display = 'block';
document.getElementById('gn61').style.display = 'block';
getIndex(gn61Data,'gn61');
}
if (num=="social1") {
resetall();
minx=558;
maxx=747;
document.getElementById('gn').style.display = 'block';
document.getElementById('gn71').style.display = 'block';
getIndex(gn71Data,'gn71');
}

floating = 1;

}
}

function callglbs(fdata, fid){
if (bdata!=0){
getIndex(fdata, fid);
}
}

function writeglb(){
if (bdata!=0){
document.write('<div id="gn">');
document.write('<div id="gn11"></div>');
document.write('<div id="gn12"></div>');
document.write('<div id="gn13"></div>');
document.write('<div id="gn21"></div>');
document.write('<div id="gn31"></div>');
document.write('<div id="gn41"></div>');
document.write('<div id="gn51"></div>');
document.write('<div id="gn52"></div>');
document.write('<div id="gn61"></div>');
document.write('<div id="gn71"></div>');
document.write('</div>');
}
if (bdata!=0){
window.document.onmousemove = mousepos;
}
}

function clearall(){
	
}

function resetall(){
if (bdata!=0){
document.getElementById('gn11').style.display = 'none';
document.getElementById('gn12').style.display = 'none';
document.getElementById('gn13').style.display = 'none';
document.getElementById('gn21').style.display = 'none';
document.getElementById('gn31').style.display = 'none';
document.getElementById('gn41').style.display = 'none';
document.getElementById('gn51').style.display = 'none';
document.getElementById('gn52').style.display = 'none';
document.getElementById('gn61').style.display = 'none';
document.getElementById('gn71').style.display = 'none';
document.getElementById('gn').style.display = 'none';
}
}
function resetinv(idata){
if (bdata!=0){
document.getElementById(idata).style.display = 'none';
if (idata=="gn13"){
minx=60;
maxx=437;
}
if (idata=="gn12"){
minx=60;
maxx=249;
}
if (idata=="gn52"){
minx=485;
maxx=674;
}
}
}

function gnon(idata){
if (bdata!=0){
document.getElementById(idata).style.backgroundImage = 'url(\'http://www.mitsubishi-motors.co.jp/share/images/global_bg_03.gif\')';
}
}
function gnof(idata){
if (bdata!=0){
document.getElementById(idata).style.backgroundImage = 'url(\'http://www.mitsubishi-motors.co.jp/share/images/global_bg_02.gif\')';
}
}

function mousepos(evt) {
if (document.all) {
X = event.clientX;
Y = event.clientY;
}else{
X = evt.clientX;
Y = evt.clientY;
}
	if (floating==1){
		if (Y<60||Y>510){
			floating = 0;
			resetall();
		} else if (60<Y&&Y<91) {
				if(X<60||X>748){
					floating = 0;
					resetall();
				}
				if(394<X && X<484) {
					floating = 0;
					resetall();
				}
		} else if (X<minx||X>maxx) {
			floating = 0;
			resetall();
		}
	}
}



function getIndex(getData, getID) {
document.getElementById(getID).innerHTML = getData;
}
