//----------------------------------------------------------------------------
	
function spnewsData(date,carnm,maintxt,url,blankck){
	
	this.date = date;
	this.carnm = carnm;
	this.maintxt = maintxt;
	this.url = url;
	this.blankck = blankck;
	
}

//----------------------------------------------------------------------------

//
//改行は内容テキスト中に<br>をいれると改行されます
//タイトルなし、内容のみでも表示可能です。


//（）内の説明-----------------------
//左から
//更新日（2桁表記必須 例：07.01.01）,
//タイトル,
//内容,
//リンク先（無い場合は空白）,
//ブランク判定（0は同ウィンドウ、1ならブランクで開く）''は付けない




//ここから編集可領域///////////////////////////////////////////////////////////////////////////////////////////////////////
spnews = new Array(
new spnewsData('07.02.23','パジェロ','ダカールラリー7連覇・通算12勝記念特別仕様車「ラリーレプリカ」発売！！','http://www.mitsubishi-motors.co.jp/pajero/index.html',0,0,0),
new spnewsData('07.02.23','デリカ Ｄ：５','雪上試乗会映像を公開！','http://www.mitsubishi-motors.co.jp/delica_d5/special/snowimp/index.html',1,0,0),
new spnewsData('07.02.08','ｅＫ スペシャルコンテンツ','ｅＫワゴン・マーブルエディションのコンテンツを追加しました。','http://new-ek.jp/',1),
new spnewsData('07.01.15','デリカ Ｄ：５ スペシャルコンテンツ','開発者インタビュー公開！','http://gran-off.jp/suvinfo/delica/index.html',1),
new spnewsData('07.01.16','ｉ（アイ）　スペシャルコンテンツ','i(アイ)を通じた様々なストーリー“i-StoryPartII”公開中。','http://mitsubishi-i.jp/index.html?id=1',1),
new spnewsData('07.01.16','ｉ（アイ）','特別仕様車“ i（アイ）1st Anniversary Edition”を発売。','http://www.mitsubishi-motors.co.jp/i/lineup/lin_02.html',0),
new spnewsData('07.01.22','デリカ Ｄ：５ スペシャルコンテンツ','“オートサロンレポート”公開！','http://gran-off.jp/suvinfo/delica/index.html',1),
new spnewsData('07.01.22','デリカ Ｄ：５ スペシャルコンテンツ','試乗インプレッションUpdate！','http://gran-off.jp/suvinfo/delica/index.html',1),
new spnewsData('07.01.31','デリカ Ｄ：５','“家族を愛する人の、タフＢＯＸ”デリカ Ｄ：５誕生！','http://www.mitsubishi-motors.co.jp/delica_d5/index.html',0)
);


//編集可領域ここまで///////////////////////////////////////////////////////////////////////////////////////////////////////


//-----



//以下変更禁止---------------------------------------------------------------------------
function boxsort(){
	
for (i=0; i<spnews.length-1; i++) {
	for (j=i+1; j<spnews.length; j++) {
	if (spnews[j].date >= spnews[i].date) {
		 n = spnews[j];
		 spnews[j] = spnews[i];
		 spnews[i] = n;
	}
	}
	}	
}

function news_write(){
 
var writetags;

document.write('<ul>\n');

boxsort();

if(spnews.length >= 0){
	for(i=0; i<spnews.length;i++){
		writetags = '<li>';
		if(spnews[i].url){
			if(spnews[i].blankck == 1){
			blankbox = 'target="_blank">';
			}else if(spnews[i].blankck == 0){
			blankbox = '>';
			}else{
			break;	
			}
			if(spnews[i].carnm){
				writetags += '<a href="'+spnews[i].url+'" '+blankbox+'『'+spnews[i].carnm+'』'+spnews[i].maintxt+'（'+spnews[i].date+'）<\/a>';
			}else{
				writetags += '<a href="'+spnews[i].url+'" '+blankbox+spnews[i].maintxt+'（'+spnews[i].date+'）<\/a>';
			}
		}else{
			if(spnews[i].carnm){
				writetags += '『'+spnews[i].carnm+'』'+spnews[i].maintxt+'（'+spnews[i].date+'）';		
			}else{
				writetags += spnews[i].maintxt+'（'+spnews[i].date+'）';		
			}
		}
		writetags += '<\/li>\n';
		document.write(writetags);
		}
}
document.write('<\/ul>');
}

