//----------------------------------------------------------------------------
var _category;


function spnewsData(pickup,date,carnm,maintxt,url,blankck,popwidth,popheight){
	this.pickup = pickup;
	this.date = date;
	this.carnm = carnm;
	this.maintxt = maintxt;
	this.url = url;
	this.blankck = blankck;
	this.popwidth = popwidth;
	this.popheight = popheight;	
}

//----------------------------------------------------------------------------

//
//改行は内容テキスト中に<br>をいれると改行されます
//


//（）内の説明-----------------------
//左から
//カーラインアップ、ピックアップ欄に表示（0：表示しない、1：表示する）←カーラインアップ専用
//更新日（2桁表記必須 例：07.01.01）（無い場合は空白、空白にしたものはソートしないため記述した位置にそのまま表示されます）,
//タイトル（無い場合は空白）,
//内容（無い場合は空白）,
//リンク先（無い場合は空白）,
//ブランク判定（0は同ウィンドウ、1はブランクで開く、2はポップアップで開く）''は付けない
//ポップアップのウィンドウサイズ（width,height（ポップアップでない場合は0））

//ポップアップサイズ例
//cmライブラリー（620,700）


//ここから編集可領域///////////////////////////////////////////////////////////////////////////////////////////////////////

//日付並び替え機能
//使用する：1、使用しない：0
//
//使用する場合は、記述の順番関係なく日付で自動的に並び替えて出力されます。使用しない場合は、記述した順番そのままで出力されます。
//
var _sortSwitch = 1;

//カーラインアップ用
carnews = new Array(
new spnewsData(0,'11.02.24','デリカD:2','を発表いたしました。','/delica_d2/index.html',0,0,0),
new spnewsData(0,'10.12.20','デリカD:5','の一部改良を発表いたしました。','/delica_d5/index.html',0,0,0),
new spnewsData(0,'10.12.20','デリカD:5','など6車種の特別仕様車「Navi Collection」を発表いたしました。','/edition/navi_collection/index.html',1,0,0),
new spnewsData(0,'10.12.20','パジェロミニ','の特別仕様車「Premium Selection」を発表いたしました。','/pajero_mini/lineup/premium_selection/index.html',1,0,0),
new spnewsData(0,'10.12.02','RVR','の特別仕様車、「1st Anniversary Edition」を発表いたしました。','/rvr/grade/1st_anniversary/index.html',1,0,0),
new spnewsData(0,'10.11.04','i-MiEV','の一部改良を発表いたしました。','/i-miev/index.html',0,0,0),
new spnewsData(0,'10.10.08','ランサーエボリューションX','の一部改良を発表いたしました。','/evo/index.html',0,0,0),
new spnewsData(0,'10.10.07','ミニカ','の一部改良を発表いたしました。','/minica/index.html ',0,0,0),
new spnewsData(0,'10.10.07','デリカD:5','の特別仕様車、「CHAMONIX」を発表しました。','/delica_d5/lineup/chamonix/index.html ',1,0,0)
);



//スペシャルコンテンツ用
splnews = new Array(
new spnewsData(0,'10.12.22','','GRAN OFF『地元 道の駅＆おすすめスポット』公開いたしました。','http://gran-off.jp/michinoeki/index.html',1,0,0),
new spnewsData(0,'10.12.10','三菱10年10万kmストーリー','公開いたしました。','http://www.mitsubishi-motors.co.jp/special/10year100kkm/ ',1,0,0)
);


//編集可領域ここまで///////////////////////////////////////////////////////////////////////////////////////////////////////


//-----



//以下変更禁止---------------------------------------------------------------------------
function boxsort(){
	
for (i=0; i<spnews.length-1; i++) {
	for (j=i+1; j<spnews.length; j++) {
	if(spnews[i].date){	
	if (spnews[j].date >= spnews[i].date) {
		 n = spnews[j];
		 spnews[j] = spnews[i];
		 spnews[i] = n;
	}
	}
	}
	}	
}

var news_write = function (_category,pickup){
 
 
spnews =  eval(_category + 'news');


var writetags;


if(_sortSwitch == 1){
boxsort();
}

if(pickup) var pickupflg = 1;
else  var pickupflg = 0;

if(_category == 'spl') document.write('<ul>');

if(spnews.length >= 0){
	for(i=0; i<spnews.length;i++){
		if(spnews[i].pickup == pickupflg){
		
		if(_category == 'spl') writetags = '<li>';
		else writetags = '<p><span>';
		
		if(spnews[i].url){
			if(spnews[i].blankck == 1){
			var blankbox = 'target="_blank">';
			}else if(spnews[i].blankck == 0){
			var blankbox = '>';
			}else if(spnews[i].blankck == 2){
			var blankbox = 'onClick=\"openWin(this.href,\'wnpopwin'+i+'\',\''+spnews[i].popwidth +'\',\''+spnews[i].popheight +'\',\'2\');return false;\" ';	
			blankbox += 'onKeyPress=\"openWin(this.href,\'wnpopwin'+i+'\',\''+spnews[i].popwidth +'\',\''+spnews[i].popheight +'\',\'2\');return false;\">';	
			}else{
			break;	
			}
			if(spnews[i].date){
				var datebox = '（'+spnews[i].date+'）';
			}else{
				var datebox = '';
			}
			if(spnews[i].carnm){
				writetags += '<a href="'+spnews[i].url+'" '+blankbox+'『'+spnews[i].carnm+'』'+spnews[i].maintxt+datebox+'</a>';
			}else{
				writetags += '<a href="'+spnews[i].url+'" '+blankbox+spnews[i].maintxt+datebox+'</a>';
			}
		}else{
			if(spnews[i].date){
				var datebox = '（'+spnews[i].date+'）';
			}else{
				var datebox = '';
			}
			if(spnews[i].carnm){
				writetags += '『'+spnews[i].carnm+'』'+spnews[i].maintxt+datebox;		
			}else{
				writetags += spnews[i].maintxt+datebox;		
			}
		}
		
		if(_category == 'spl') writetags += '</li>';
		else writetags += '</span></p>';
		document.write(writetags);
		}
	}
}
if(_category == 'spl') document.write('</ul>');

}

