// update:2005/12　>>add Flash Player Detection Kit 


// Globals
// Major version of Flash required
var requiredMajorVersion = 8;
// Minor version of Flash required
var requiredMinorVersion = 0;
// Minor version of Flash required
var requiredRevision = 0;
// the version of javascript supported
var jsVersion = 1.0;

var isIE  = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
var isWin = (navigator.appVersion.toLowerCase().indexOf("win") != -1) ? true : false;
var isOpera = (navigator.userAgent.indexOf("Opera") != -1) ? true : false;
jsVersion = 1.1;
// JavaScript helper required to detect Flash Player PlugIn version information
function JSGetSwfVer(i){
	// NS/Opera version >= 3 check for Flash plugin in plugin array
	if (navigator.plugins != null && navigator.plugins.length > 0) {
		if (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]) {
			var swVer2 = navigator.plugins["Shockwave Flash 2.0"] ? " 2.0" : "";
      		var flashDescription = navigator.plugins["Shockwave Flash" + swVer2].description;
			descArray = flashDescription.split(" ");
			tempArrayMajor = descArray[2].split(".");
			versionMajor = tempArrayMajor[0];
			versionMinor = tempArrayMajor[1];
			if ( descArray[3] != "" ) {
				tempArrayMinor = descArray[3].split("r");
			} else {
				tempArrayMinor = descArray[4].split("r");
			}
      		versionRevision = tempArrayMinor[1] > 0 ? tempArrayMinor[1] : 0;
            flashVer = versionMajor + "." + versionMinor + "." + versionRevision;
      	} else {
			flashVer = -1;
		}
	}
	// MSN/WebTV 2.6 supports Flash 4
	else if (navigator.userAgent.toLowerCase().indexOf("webtv/2.6") != -1) flashVer = 4;
	// WebTV 2.5 supports Flash 3
	else if (navigator.userAgent.toLowerCase().indexOf("webtv/2.5") != -1) flashVer = 3;
	// older WebTV supports Flash 2
	else if (navigator.userAgent.toLowerCase().indexOf("webtv") != -1) flashVer = 2;
	// Can't detect in all other cases
	else {
		
		flashVer = -1;
	}
	return flashVer;
} 
// When called with reqMajorVer, reqMinorVer, reqRevision returns true if that version or greater is available
function DetectFlashVer(reqMajorVer, reqMinorVer, reqRevision) {
 	reqVer = parseFloat(reqMajorVer + "." + reqRevision);
   	// loop backwards through the versions until we find the newest version	
	for (i=25;i>0;i--) {	
		if (isIE && isWin && !isOpera) {
			versionStr = VBGetSwfVer(i);
		} else {
			versionStr = JSGetSwfVer(i);		
		}
		if (versionStr == -1 ) { 
			return false;
		} else if (versionStr != 0) {
			if(isIE && isWin && !isOpera) {
				tempArray         = versionStr.split(" ");
				tempString        = tempArray[1];
				versionArray      = tempString .split(",");				
			} else {
				versionArray      = versionStr.split(".");
			}
			versionMajor      = versionArray[0];
			versionMinor      = versionArray[1];
			versionRevision   = versionArray[2];
			
			versionString     = versionMajor + "." + versionRevision;   // 7.0r24 == 7.24
			versionNum        = parseFloat(versionString);
        	// is the major.revision >= requested major.revision AND the minor version >= requested minor
			if (versionMajor == reqMajorVer) {
				if (versionMinor == reqMinorVer) {
					return (versionRevision   >= reqRevision);
				} else {
					return (versionMinor   > reqMinorVer);
				}
			} else {
				return (versionMajor   > reqMajorVer);
			}
		}
	}	
}


function setObject(attribute){

	if (attribute.plgVer) requiredMajorVersion = attribute.plgVer;

	var hasReqestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);

	if (!attribute.src) return false;

	if (hasReqestedVersion) {

		if (!attribute.menu) attribute.menu = "false";
		if (!attribute.quality) attribute.quality = "high";
		if (!attribute.loop) attribute.loop = "false";
		if (!attribute.bgcolor) attribute.bgcolor = "#ffffff";
		if (!attribute.align) attribute.align = "middle";
		if (!attribute.allowScriptAccess) attribute.allowScriptAccess = "always";
		if (!attribute.name) attribute.name = "movie1";
		if (!attribute.altContent) attribute.altContent = "当サイトをご覧いただくには、最新のmacromedia Flash Player（無料）が必要です。<br>ご使用のパソコンにインストールされていない場合は、<a href=\"http://www.macromedia.com/jp/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash&Lang=Japanese\" target=\"_blank\">macromediaのサイト</a>からダウンロードしてください。";

		var oeTags ='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'
		+ 'width="' + attribute.width + '" height="'+ attribute.height + '"'
		+ 'codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab">'
		+ '<param name="movie" value="' + attribute.src + '">'
		+ '<param name="menu" value="' + attribute.menu + '">'
		+ '<param name="quality" value="' + attribute.quality + '">'
		+ '<param name="bgcolor" value="' + attribute.bgcolor + '">'
		+ '<param name="allowScriptAccess" value="' + attribute.allowScriptAccess + '">';
		
		if (attribute.wmode) oeTags += '<param name="wmode" value="' + attribute.wmode + '">';
		
		oeTags += '<embed src="' + attribute.src + '" menu="' + attribute.menu + '" quality="' + attribute.quality + '" bgcolor="' + attribute.bgcolor + '" '
		+ ' width="' + attribute.width + '" height="' + attribute.height + '" name="' + attribute.name + '" align="' + attribute.align + '"'
		+ ' play="true"'
		+ ' loop="' + attribute.loop + '"'
		+ ' quality="' + attribute.quality + '"';
		
		if (attribute.wmode) oeTags += ' wmode="' + attribute.wmode + '"';
		
		oeTags += ' allowScriptAccess="' + attribute.allowScriptAccess + '"'
		+ ' type="application/x-shockwave-flash"'
		+ ' pluginspage="http://www.macromedia.com/go/getflashplayer">'
		+ '<\/embed>'
		+ '<\/object>';

		document.write(oeTags);

	} else{
		if(attribute.altContent){
			document.write(attribute.altContent);
		}
	}
}

